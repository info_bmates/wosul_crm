<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('menus')->insert([
            'type' => 'MAIN_MENU',
            'menu_key' => 'MM_WEBSITE',
            'label' => 'Website',
            'route' => '',
            'parent' => 0,
            'sort_order' => 2,
            'icon' => 'fa fa-clone',
            'image' => '',
            'is_restaurant_menu' => 0,
            'status' => 1,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'MM_WEBSITE')->delete();
    }
};
