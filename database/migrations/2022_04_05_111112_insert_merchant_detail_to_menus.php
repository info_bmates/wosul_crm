<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parent_id = DB::table('menus')->select('id')->where('menu_key', 'MM_MERCHANT')->first();

        DB::table('menus')->insert([
            'type' => 'ACTIONS',
            'menu_key' => 'A_VIEW_MERCHANT',
            'label' => 'View Merchant',
            'route' => '',
            'parent' => $parent_id->id,
            'sort_order' => 1,
            'icon' => 'fa fa-user'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'A_VIEW_MERCHANT')->delete();
    }
};
