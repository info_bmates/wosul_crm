<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('website_settings')->insert(
            [
                'key' => 'ENABLE_PAYMENT_GATEWAY',
                'type' => 'TEXT',
                'value' => 'false',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('website_settings')->where('key', 'ENABLE_PAYMENT_GATEWAY')->delete();
    }
};
