<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOrderTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('order_amount', 10, 2)->change();
            $table->decimal('tax_rate', 10, 2)->change();
            $table->decimal('tax_amount', 10, 2)->change();
            $table->decimal('discount_rate', 10, 2)->change();
            $table->decimal('discount_amount', 10, 2)->change();
            $table->decimal('total_amount', 10, 2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('order_amount', 5, 2)->change();
            $table->decimal('tax_rate', 5, 2)->change();
            $table->decimal('tax_amount', 5, 2)->change();
            $table->decimal('discount_rate', 5, 2)->change();
            $table->decimal('discount_amount', 5, 2)->change();
            $table->decimal('total_amount', 5, 2)->change();
        });
    }
}
