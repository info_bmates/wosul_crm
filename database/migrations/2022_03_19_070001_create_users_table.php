<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slack', 30)->unique();
            $table->string('name', 250);
            $table->string('email', 150)->unique();
            $table->string('password', 100);
            $table->string('phone', 15)->nullable();
            $table->integer('role_id')->nullable();
            $table->tinyInteger('status')->default(0)->index();
            $table->tinyInteger('is_admin')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('users')->insert([
            'slack' => Str::random(10),
            'name' => 'Admin',
            'email' => 'admin@wosul.sa',
            'password' => Hash::make('123456'),
            'phone' => '',
            'role_id' => 1,
            'status' => 1,
            'is_admin' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
