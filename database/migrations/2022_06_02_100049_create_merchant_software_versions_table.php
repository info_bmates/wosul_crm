<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_software_versions', function (Blueprint $table) {
            $table->id();
            $table->integer('merchant_id');
            $table->enum('os', [1, 2])->comment('1-IoS, 2-Android');
            $table->string('unique_deviceid', 50);
            $table->string('version', 50);
            $table->string('device_token', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_software_version');
    }
};
