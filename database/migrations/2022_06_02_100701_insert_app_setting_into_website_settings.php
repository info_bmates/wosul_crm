<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('website_settings')->insert(
            [
                'key' => 'IOS_APP_VERSION',
                'type' => 'TEXT',
            ]
        );

        DB::table('website_settings')->insert(
            [
                'key' => 'ANDROID_APP_VERSION',
                'type' => 'TEXT',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('website_settings')->where('key', 'IOS_APP_VERSION')->delete();
        DB::table('website_settings')->where('key', 'ANDROID_APP_VERSION')->delete();
    }
};
