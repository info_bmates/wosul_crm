<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_marketplace_specifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('marketplace_id')->constrained('website_marketplaces');
            $table->string('name');
            $table->string('name_ar');
            $table->string('value');
            $table->string('value_ar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_marketplace_specifications');
    }
};
