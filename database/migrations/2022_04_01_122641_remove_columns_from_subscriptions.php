<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('url_ar');
            $table->dropColumn('color_code');
            $table->dropColumn('is_live');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('url')->nullable()->after('discount_description_ar');
            $table->string('url_ar')->nullable()->after('url');
            $table->string('color_code')->nullable()->after('url_ar');
            $table->string('is_live')->nullable()->after('is_featured');
        });
    }
};
