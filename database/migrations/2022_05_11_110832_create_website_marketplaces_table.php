<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_marketplaces', function (Blueprint $table) {
            $table->id();
            $table->string('slack', 50);
            $table->string('title');
            $table->string('title_ar');
            $table->string('thumb_image');
            $table->string('banner_image')->nullable();
            $table->string('short_description');
            $table->string('short_description_ar');
            $table->text('long_description');
            $table->text('long_description_ar');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_marketplaces');
    }
};
