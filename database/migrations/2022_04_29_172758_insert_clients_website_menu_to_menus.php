<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parent = DB::table('menus')->select('id')->where('menu_key', 'MM_WEBSITE')->first();

        DB::table('menus')->insert(
            [
                'type' => 'SUB_MENU',
                'menu_key' => 'SM_WEBSITE_CLIENT',
                'label' => 'Clients',
                'route' => 'website/clients',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => '',
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );

        $parent = DB::table('menus')->select('id')->where('menu_key', 'SM_WEBSITE_CLIENT')->first();

        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_ADD_WEBSITE_CLIENT',
                'label' => 'Add Client',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_EDIT_WEBSITE_CLIENT',
                'label' => 'Edit Client',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_DELETE_WEBSITE_CLIENT',
                'label' => 'Delete Client',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_VIEW_WEBSITE_CLIENT',
                'label' => 'View Client',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'SM_WEBSITE_CLIENT')->delete();
        DB::table('menus')->where('menu_key', 'A_ADD_WEBSITE_CLIENT')->delete();
        DB::table('menus')->where('menu_key', 'A_EDIT_WEBSITE_CLIENT')->delete();
        DB::table('menus')->where('menu_key', 'A_DELETE_WEBSITE_CLIENT')->delete();
        DB::table('menus')->where('menu_key', 'A_VIEW_WEBSITE_CLIENT')->delete();
    }
};
