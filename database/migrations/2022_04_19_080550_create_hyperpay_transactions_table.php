<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hyperpay_transactions', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('merchant_id');
            $table->tinyInteger('checkout_id')->index();
            $table->decimal('amount', 5, 2);
            $table->string('currency');
            $table->text('data')->nullable();
            $table->text('trackable_data');
            $table->text('billing_address')->nullable();
            $table->text('card_details')->nullable();
            $table->string('brand');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hyperpay_transactions');
    }
};
