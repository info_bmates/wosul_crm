<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parent = DB::table('menus')->select('id')->where('menu_key', 'MM_WEBSITE')->first();

        DB::table('menus')->insert(
            [
                'type' => 'SUB_MENU',
                'menu_key' => 'SM_WEBSITE_GALLERY',
                'label' => 'Gallery',
                'route' => 'website/gallery',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => '',
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );

        $parent = DB::table('menus')->select('id')->where('menu_key', 'SM_WEBSITE_GALLERY')->first();

        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_ADD_WEBSITE_GALLERY',
                'label' => 'Add Gallery',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_EDIT_WEBSITE_GALLERY',
                'label' => 'Edit Gallery',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_DELETE_WEBSITE_GALLERY',
                'label' => 'Delete Gallery',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_VIEW_WEBSITE_GALLERY',
                'label' => 'View Gallery',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'SM_WEBSITE_GALLERY')->delete();
        DB::table('menus')->where('menu_key', 'A_ADD_WEBSITE_GALLERY')->delete();
        DB::table('menus')->where('menu_key', 'A_EDIT_WEBSITE_GALLERY')->delete();
        DB::table('menus')->where('menu_key', 'A_DELETE_WEBSITE_GALLERY')->delete();
        DB::table('menus')->where('menu_key', 'A_VIEW_WEBSITE_GALLERY')->delete();
    }
};
