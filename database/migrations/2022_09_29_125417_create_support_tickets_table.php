<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')->nullable();
            $table->tinyInteger('merchant_id');
            $table->tinyInteger('ticket_type')->comment('1-Inquiry,2-Suggestion,3-Complaint,4-Technical Problem,5-Technical Support');
            $table->string('user_name')->nullable();
            $table->string('email')->nullable();
            $table->tinyInteger('priority')->default(1)->comment('0-Low,1-Medium,2-High');
            $table->date('reporting_date');
            $table->string('attachment')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0-Open,1-Under Processing,2-Closed');
            $table->tinyInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
};
