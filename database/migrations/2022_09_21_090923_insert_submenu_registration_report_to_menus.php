<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parent_menu = DB::table('menus')->where('menu_key','MM_REPORT')->first();

        if(isset($parent_menu)){
            
            DB::table('menus')->insert(
                [
                    'type' => 'SUB_MENU',
                    'menu_key' => 'SM_REGISTRATION_REPORT',
                    'label' => 'Registration Report',
                    'route' => 'report/registration_report',
                    'parent' => $parent_menu->id,
                    'sort_order' => 1,
                    'icon' => 'fa fa-user',
                    'image' => '',
                    'is_restaurant_menu' => 0,
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]
            );
    
            $sub_menu = DB::table('menus')->where('menu_key','SM_REGISTRATION_REPORT')->first();
    
            if(isset($sub_menu)){
    
                DB::table('role_menus')->insert([
                    'role_id' => 1,
                    'menu_id' => $sub_menu->id
                ]); 
                
                DB::table('user_menus')->insert([
                    'user_id' => 1,
                    'menu_id' => $sub_menu->id
                ]); 
    
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'SM_REGISTRATION_REPORT')->delete();
    }
};
