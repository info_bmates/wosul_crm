<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabby_transactions', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('merchant_id');
            $table->tinyInteger('payment_id')->index();
            $table->decimal('amount', 5, 2);
            $table->string('currency');
            $table->text('trackable_data');
            $table->text('tabby_details')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabby_transactions');
    }
};
