<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('role_menus')->delete();

        $all_menus = \DB::table('menus')->get();

        foreach ($all_menus as $menu) {

            \DB::table('role_menus')->insert(
                [
                    'role_id' => 1,
                    'menu_id' => $menu->id
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('role_menus')->delete();
    }
};
