<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('website_settings')->insert(
            [
                'key' => 'SUPPORT_CONTACT_NUMBER',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'SUPPORT_EMAIL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'INFO_EMAIL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'VISITING_ADDRESS',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'FACEBOOK_URL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'INSTAGRAM_URL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'TWITTER_URL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'YOUTUBE_URL',
                'type' => 'TEXT',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'HEADER_LOGO',
                'type' => 'IMAGE',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'FOOTER_LOGO',
                'type' => 'IMAGE',
            ]
        );
        DB::table('website_settings')->insert(
            [
                'key' => 'FAVICON_ICON',
                'type' => 'IMAGE',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('website_settings')->where('key', 'SUPPORT_CONTACT_NUMBER')->delete();
        DB::table('website_settings')->where('key', 'SUPPORT_EMAIL')->delete();
        DB::table('website_settings')->where('key', 'INFO_EMAIL')->delete();
        DB::table('website_settings')->where('key', 'VISITING_ADDRESS')->delete();
        DB::table('website_settings')->where('key', 'FACEBOOK_URL')->delete();
        DB::table('website_settings')->where('key', 'INSTAGRAM_URL')->delete();
        DB::table('website_settings')->where('key', 'TWITTER_URL')->delete();
        DB::table('website_settings')->where('key', 'YOUTUBE_URL')->delete();
    }
};
