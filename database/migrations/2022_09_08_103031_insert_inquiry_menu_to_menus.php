<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('menus')->insert(
            [
                'type' => 'MAIN_MENU',
                'menu_key' => 'MM_INQUIRY',
                'label' => 'Inquiry',
                'route' => '',
                'parent' => 0,
                'sort_order' => 1,
                'icon' => 'fa fa-user',
                'image' => '',
                'is_restaurant_menu' => 0,
                'status' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );

        $menu_id = DB::table('menus')->where('menu_key','MM_INQUIRY')->first();

        if(isset($menu_id)){

            DB::table('role_menus')->insert([
                'role_id' => 1,
                'menu_id' => $menu_id->id
            ]); 
            
            DB::table('user_menus')->insert([
                'user_id' => 1,
                'menu_id' => $menu_id->id
            ]); 

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'MM_INQUIRY')->delete();
    }
};
