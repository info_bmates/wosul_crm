<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->integer('user_type')->after('recommendation')->nullable();
            $table->integer('merchant_business')->after('user_type')->nullable();
            $table->string('other_merchant_business')->after('merchant_business')->nullable();
            $table->string('additional_phone_number')->after('phone_number')->nullable();
            $table->string('city')->after('email');
            $table->string('invoice_number')->after('referral_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchants', function (Blueprint $table) {
            $table->dropColumn('user_type');
            $table->dropColumn('merchant_business');
            $table->dropColumn('other_merchant_business');
            $table->dropColumn('additional_phone_number');
            $table->dropColumn('city');
            $table->dropColumn('invoice_number');
        });
    }
};
