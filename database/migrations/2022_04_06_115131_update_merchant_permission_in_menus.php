<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('menus')->where('menu_key', 'MM_MERCHANT')->update(['route' => '']);
        DB::table('menus')->where('menu_key', 'A_VIEW_MERCHANT')->update(['label' => 'View Merchants', 'route' => 'merchants']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'MM_MERCHANT')->update(['route' => 'merchants']);
        DB::table('menus')->where('menu_key', 'A_VIEW_MERCHANT')->update(['label' => 'View Merchant', 'route' => '']);
    }
};
