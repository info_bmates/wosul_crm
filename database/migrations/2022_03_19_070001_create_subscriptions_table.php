<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slack', 50);
            $table->string('title', 50)->nullable();
            $table->text('title_ar')->nullable();
            $table->mediumText('short_description')->nullable();
            $table->text('short_description_ar')->nullable();
            $table->integer('plan_tenure')->comment('1-Month, 2 Year');
            $table->string('currency', 50);
            $table->decimal('amount', 10);
            $table->decimal('discount', 10);
            $table->mediumText('discount_description')->nullable();
            $table->text('discount_description_ar')->nullable();
            $table->mediumText('url')->nullable();
            $table->mediumText('url_ar')->nullable();
            $table->string('color_code');
            $table->integer('is_featured')->nullable()->default(0);
            $table->integer('is_live')->default(0);
            $table->integer('status');
            $table->softDeletes();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
