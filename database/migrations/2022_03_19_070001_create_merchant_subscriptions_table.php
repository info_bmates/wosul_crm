<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_subscriptions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('subscription_id');
            $table->integer('merchant_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('status')->default(0)->comment('0-Pending,1-Active,2-Inactive,3-Expired');
            $table->softDeletes()->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_subscriptions');
    }
}
