<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_plans', function (Blueprint $table) {
            $table->id();
            $table->string('plan_title', 30)->unique();
            $table->string('plan_title_ar', 30)->unique();
            $table->decimal('rate', 10);
            $table->decimal('discount', 10);
            $table->decimal('amount', 10);
            $table->integer('is_featured')->nullable()->default(0);
            $table->integer('status')->default(1)->comment('1-Active,2-Inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_plans');
    }
};
