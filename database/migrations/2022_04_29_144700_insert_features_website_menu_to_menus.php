<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parent = DB::table('menus')->select('id')->where('menu_key', 'MM_WEBSITE')->first();

        DB::table('menus')->insert(
            [
                'type' => 'SUB_MENU',
                'menu_key' => 'SM_WEBSITE_FEATURE',
                'label' => 'Features',
                'route' => 'website/features',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => '',
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );

        $parent = DB::table('menus')->select('id')->where('menu_key', 'SM_WEBSITE_FEATURE')->first();

        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_ADD_WEBSITE_FEATURE',
                'label' => 'Add Feature',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_EDIT_WEBSITE_FEATURE',
                'label' => 'Edit Feature',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_DELETE_WEBSITE_FEATURE',
                'label' => 'Delete Feature',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
        DB::table('menus')->insert(
            [
                'type' => 'ACTIONS',
                'menu_key' => 'A_VIEW_WEBSITE_FEATURE',
                'label' => 'View Feature',
                'route' => '',
                'parent' => $parent->id,
                'sort_order' => 1,
                'icon' => 'fa-solid fa-asterisk',
                'image' => NULL,
                'is_restaurant_menu' => 0,
                'status' => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('menus')->where('menu_key', 'SM_WEBSITE_FEATURE')->delete();
        DB::table('menus')->where('menu_key', 'A_ADD_WEBSITE_FEATURE')->delete();
        DB::table('menus')->where('menu_key', 'A_EDIT_WEBSITE_FEATURE')->delete();
        DB::table('menus')->where('menu_key', 'A_DELETE_WEBSITE_FEATURE')->delete();
        DB::table('menus')->where('menu_key', 'A_VIEW_WEBSITE_FEATURE')->delete();
    }
};
