<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('slack', 50);
            $table->integer('subscription_id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('password');
            $table->string('company_name');
            $table->string('company_url');
            $table->mediumText('address')->nullable();
            $table->string('promo_code')->nullable();
            $table->string('recommendation')->nullable();
            $table->tinyInteger('is_central')->default(1)->comment('0-not a central source, 1 - central source');
            $table->integer('status')->default(1);
            $table->softDeletes();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
