<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('order_number');
            $table->integer('merchant_id');
            $table->string('address_line1', 255);
            $table->string('address_line2', 255);
            $table->string('city', 255);
            $table->string('zipcode', 255);
            $table->decimal('order_amount', 5, 2);
            $table->decimal('tax_rate', 5, 2);
            $table->decimal('tax_amount', 5, 2);
            $table->decimal('discount_rate', 5, 2);
            $table->decimal('discount_amount', 5, 2);
            $table->decimal('total_amount', 5, 2);
            $table->tinyInteger('payment_status')->comment('0-Pending,1-Paid')->default(0);
            $table->foreignId('payment_method_id')->constrained();
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
