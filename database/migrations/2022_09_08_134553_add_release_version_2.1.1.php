<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('releases')->insert([
            'version' => '2.1.1',
            'title' => 'Bug fixes from v2.1.0',
            'release_notes' => '<div class="csg-wrapper" style="font-size: 14px; font-weight: 400; line-height: 24px; vertical-align: baseline;"><h1 class="csg-h1" style="font-style: inherit; color: #172B4D; font-weight: 600; margin-bottom: 0; font-size: 23px; line-height: 1.1034; margin-top: 40px; letter-spacing: -0.01em;">2.1.1</h1><h3 class="csg-h3" style="font-style: inherit; color: #172B4D; font-weight: 600; margin-bottom: 0; font-size: 16px; line-height: 1.2; margin-top: 32px; font-weight: 500; letter-spacing: -0.008em;">Bug</h3><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-133" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-133</a> Item subtotal of product list in invoice return is wrong, correct it.</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-132" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-132</a> Update return list report to remove usage of is_wastage field</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-131" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-131</a> Allow to accept nullable value for discount percentage field in discount_codes table</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-128" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-128</a> Write migration for updating empty applicable fields in category table</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-126" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-126</a> Products are not loading while scrolling add order page</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-125" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-125</a> Product wise sales report not downloading</p><p class="csg-p" style="margin: 0; padding: 0px; margin-bottom: 7px; padding-top: 7px; mso-line-height-rule: exactly; line-height: 24px; font-size: 14px;"><a href="https://wosul.atlassian.net/browse/POS-124" class="csg-mark-link" style="border: none; background: transparent; color: #0052cc; text-decoration: none;">POS-124</a> Invoice detail page not loading</p></div>',
            'release_date' => '2022-08-12' 
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('releases')->where('version','2.1.1')->delete();
    }
};
