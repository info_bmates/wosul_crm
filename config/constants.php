<?php

return [

    /* 
        default date time configurations
    */

    'date_time_format' => 'Y-m-d h:i:s',

    /* 
        pagination attributes for listing pages and datatables
    */

    'pagination' => [
        'items_per_page' => 25
    ],

    'upload' => [
        'reports' => [
            'default' => '',
            'dir' => 'reports/',
            'view_path' => '/storage/reports/',
            'upload_path' => 'storage/reports/'
        ],
    ]
];
