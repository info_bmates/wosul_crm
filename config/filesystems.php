<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DISK', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been set up for each driver as an example of the required values.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
            'throw' => false,
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
            'throw' => false,
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
            'throw' => false,
        ],

        'device' => [
            'driver' => 'local',
            'root' => storage_path('app/public/device'),
            'url' => env('APP_URL') . '/storage/device',
            'visibility' => 'public',
        ],

        'website_service' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_service'),
            'url' => env('APP_URL') . '/storage/website_service',
            'visibility' => 'public',
        ],

        'website_feature' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_feature'),
            'url' => env('APP_URL') . '/storage/website_feature',
            'visibility' => 'public',
        ],

        'website_review' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_review'),
            'url' => env('APP_URL') . '/storage/website_review',
            'visibility' => 'public',
        ],

        'website_gallery' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_gallery'),
            'url' => env('APP_URL') . '/storage/website_gallery',
            'visibility' => 'public',
        ],

        'website_client' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_client'),
            'url' => env('APP_URL') . '/storage/website_client',
            'visibility' => 'public',
        ],

        'website_partner' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_partner'),
            'url' => env('APP_URL') . '/storage/website_partner',
            'visibility' => 'public',
        ],

        'website_setting' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_setting'),
            'url' => env('APP_URL') . '/storage/website_setting',
            'visibility' => 'public',
        ],

        'website_marketplace' => [
            'driver' => 'local',
            'root' => storage_path('app/public/website_marketplace'),
            'url' => env('APP_URL') . '/storage/website_marketplace',
            'visibility' => 'public',
        ],
        
        'reports' => [
            'driver' => 'local',
            'root' => storage_path('app/public/reports'),
            'url' => env('APP_URL') . '/storage/reports',
            'visibility' => 'public',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
