class CommonDatatableLanguage {

    commonDatatable() {
        return {
            sSearch: "Search",
            sLengthMenu:   "Show _MENU_ entries",
            sInfo:  "Showing _START_ to _END_ of _TOTAL_ entries",
            sZeroRecords:   "No matching records found",
            sProcessing:  "Processing...",
            sLoadingRecords: "Loading...",
            sEmptyTable:  "No data available in table",
            sFirst: "First",
            sLast:  "Last",
            sPrevious:  "Previous",
            sNext:   "Next",
            sInfoE: "Showing 0 to 0 of 0 entries",
            sInfoFiltered: "(filtered from _MAX_ total entries)"
        }
    }

}
