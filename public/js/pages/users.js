class Users{
    load_listing_table(category_type,commonDatatablesData){
        "use strict";

        // console.log(category_type);
        var listing_table = $('#listing-table').DataTable({
            ajax: {
                url  : '/api/users',
                type : 'POST',
                data : {
                    access_token : localStorage.getItem('access_token'),
                }
            },
            oLanguage: commonDatatablesData,
            columns: [
                { name: 'user.name' },
                { name: 'user.email' },
            ],
            order: [[ 1, "desc" ]],
            columnDefs: [
                { "orderable": false, "targets": [2] }
            ]
        });
    }
}