<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [App\Http\Controllers\API\RegisterAPI::class, 'store']);

Route::group(['middleware' => 'auth:api_user'], function () {
    // Role
    Route::get('/roles', [App\Http\Controllers\API\RoleAPI::class, 'index']);
    Route::post('/role/add', [App\Http\Controllers\API\RoleAPI::class, 'store']);
    Route::post('/role/update', [App\Http\Controllers\API\RoleAPI::class, 'update']);

    // User
    Route::get('/users', [App\Http\Controllers\API\UserAPI::class, 'index']);
    Route::post('/user/add', [App\Http\Controllers\API\UserAPI::class, 'store']);
    Route::post('/user/update', [App\Http\Controllers\API\UserAPI::class, 'update']);
    Route::post('/user/switch_mode', [App\Http\Controllers\API\UserAPI::class, 'switchThemeMode']);

    // Merchant
    Route::get('/merchants', [App\Http\Controllers\API\MerchantAPI::class, 'index']);
    Route::post('/merchant/update_status', [App\Http\Controllers\API\MerchantAPI::class, 'update_status']);

    // Merchant Store 
    Route::post('merchant/store/update_status', [App\Http\Controllers\API\MerchantStoreAPI::class, 'update_status']);

    // Merchant User 
    Route::post('merchant/user/update_status', [App\Http\Controllers\API\MerchantUserAPI::class, 'update_status']);

    // Device
    Route::get('/devices', [App\Http\Controllers\API\DeviceAPI::class, 'index']);
    Route::post('/device/add', [App\Http\Controllers\API\DeviceAPI::class, 'store']);
    Route::post('/device/update', [App\Http\Controllers\API\DeviceAPI::class, 'update']);

    // Subscription
    Route::get('/subscriptions', [App\Http\Controllers\API\SubscriptionAPI::class, 'index']);
    Route::post('/subscription/add', [App\Http\Controllers\API\SubscriptionAPI::class, 'store']);
    Route::post('/subscription/update', [App\Http\Controllers\API\SubscriptionAPI::class, 'update']);
    Route::post('/subscription/menu/update', [App\Http\Controllers\API\SubscriptionAPI::class, 'update_menus']);

    // Website Service
    Route::get('/website/services', [App\Http\Controllers\API\WebsiteServiceAPI::class, 'index']);
    Route::post('/website/service/add', [App\Http\Controllers\API\WebsiteServiceAPI::class, 'store']);
    Route::post('/website/service/update', [App\Http\Controllers\API\WebsiteServiceAPI::class, 'update']);

    // Website Feature
    Route::get('/website/features', [App\Http\Controllers\API\WebsiteFeatureAPI::class, 'index']);
    Route::post('/website/feature/add', [App\Http\Controllers\API\WebsiteFeatureAPI::class, 'store']);
    Route::post('/website/feature/update', [App\Http\Controllers\API\WebsiteFeatureAPI::class, 'update']);

    // Website Review
    Route::get('/website/reviews', [App\Http\Controllers\API\WebsiteReviewAPI::class, 'index']);
    Route::post('/website/review/add', [App\Http\Controllers\API\WebsiteReviewAPI::class, 'store']);
    Route::post('/website/review/update', [App\Http\Controllers\API\WebsiteReviewAPI::class, 'update']);

    // Website Gallery
    Route::get('/website/gallery', [App\Http\Controllers\API\WebsiteGalleryAPI::class, 'index']);
    Route::post('/website/gallery/add', [App\Http\Controllers\API\WebsiteGalleryAPI::class, 'store']);
    Route::post('/website/gallery/update', [App\Http\Controllers\API\WebsiteGalleryAPI::class, 'update']);

    // Website Clients
    Route::get('/website/clients', [App\Http\Controllers\API\WebsiteClientAPI::class, 'index']);
    Route::post('/website/client/add', [App\Http\Controllers\API\WebsiteClientAPI::class, 'store']);
    Route::post('/website/client/update', [App\Http\Controllers\API\WebsiteClientAPI::class, 'update']);

    // Website Partners
    Route::get('/website/partners', [App\Http\Controllers\API\WebsitePartnerAPI::class, 'index']);
    Route::post('/website/partner/add', [App\Http\Controllers\API\WebsitePartnerAPI::class, 'store']);
    Route::post('/website/partner/update', [App\Http\Controllers\API\WebsitePartnerAPI::class, 'update']);

    // Website Setting
    Route::post('/website/setting/update', [App\Http\Controllers\API\WebsiteSettingAPI::class, 'update']);

    // Website Marketplaces
    Route::get('/website/marketplaces', [App\Http\Controllers\API\WebsiteMarketplaceAPI::class, 'index']);
    Route::post('/website/marketplace/add', [App\Http\Controllers\API\WebsiteMarketplaceAPI::class, 'store']);
    Route::post('/website/marketplace/update', [App\Http\Controllers\API\WebsiteMarketplaceAPI::class, 'update']);
    
    // Reports
    Route::get('/report/engagement_report', [App\Http\Controllers\API\ReportAPI::class, 'engagement_report']);
    Route::get('/report/revenue_report', [App\Http\Controllers\API\ReportAPI::class, 'revenue_report']);
    Route::get('/report/registration_report', [App\Http\Controllers\API\ReportAPI::class, 'registration_report']);

    // Other
    Route::get('/whatsapp_visits', [App\Http\Controllers\API\InquiryAPI::class, 'whatsapp_visits']);
    Route::get('/demo_requests', [App\Http\Controllers\API\InquiryAPI::class, 'demo_requests']);

});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
