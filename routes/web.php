<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Authentication & Registration */

App::setLocale('ar');

Route::get('/', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
Route::get('/login', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
Route::post('/login', [App\Http\Controllers\LoginController::class, 'authenticate']);
Route::get('/get/user', [App\Http\Controllers\LoginController::class, 'getUser']);
Route::get('/logout', [App\Http\Controllers\LoginController::class, 'logout']);

/* After Login Routes */

Route::group(['middleware' => 'authentication'], function () {

    /* Dashboard */
    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

    /* User */
    Route::get('/users', [App\Http\Controllers\UserController::class, 'index']);
    Route::get('/user/menus', [App\Http\Controllers\UserController::class, 'getUserMenus']);
    Route::get('/user/add/{slack?}', [App\Http\Controllers\UserController::class, 'add']);

    /* Role */
    Route::get('/roles', [App\Http\Controllers\RoleController::class, 'index']);
    Route::get('/role/add/{slack?}', [App\Http\Controllers\RoleController::class, 'add']);

    /* Merchant */
    Route::get('/merchants', [App\Http\Controllers\MerchantController::class, 'index']);
    Route::get('/merchant/{slack}', [App\Http\Controllers\MerchantController::class, 'show']);

    /* Device */
    Route::get('/devices', [App\Http\Controllers\DeviceController::class, 'index']);
    Route::get('/device/add/{slack?}', [App\Http\Controllers\DeviceController::class, 'add']);

    /* Subscription */
    Route::get('/subscriptions', [App\Http\Controllers\SubscriptionController::class, 'index']);
    Route::get('/subscription/add/{slack?}', [App\Http\Controllers\SubscriptionController::class, 'add']);
    Route::get('/subscription/{slack}', [App\Http\Controllers\SubscriptionController::class, 'show']);
    Route::get('/subscription/menus/{slack}', [App\Http\Controllers\SubscriptionController::class, 'menus']);

    /* Website Services */
    Route::get('/website/services', [App\Http\Controllers\WebsiteServiceController::class, 'index']);
    Route::get('/website/service/add/{slack?}', [App\Http\Controllers\WebsiteServiceController::class, 'add']);

    /* Website Features */
    Route::get('/website/features', [App\Http\Controllers\WebsiteFeatureController::class, 'index']);
    Route::get('/website/feature/add/{slack?}', [App\Http\Controllers\WebsiteFeatureController::class, 'add']);

    /* Website Reviews */
    Route::get('/website/reviews', [App\Http\Controllers\WebsiteReviewController::class, 'index']);
    Route::get('/website/review/add/{slack?}', [App\Http\Controllers\WebsiteReviewController::class, 'add']);

    /* Website Gallery */
    Route::get('/website/gallery', [App\Http\Controllers\WebsiteGalleryController::class, 'index']);
    Route::get('/website/gallery/add/{slack?}', [App\Http\Controllers\WebsiteGalleryController::class, 'add']);

    /* Website Clients */
    Route::get('/website/clients', [App\Http\Controllers\WebsiteClientController::class, 'index']);
    Route::get('/website/client/add/{slack?}', [App\Http\Controllers\WebsiteClientController::class, 'add']);

    /* Website Partners */
    Route::get('/website/partners', [App\Http\Controllers\WebsitePartnerController::class, 'index']);
    Route::get('/website/partner/add/{slack?}', [App\Http\Controllers\WebsitePartnerController::class, 'add']);

    /* Website Setting */
    Route::get('/website/setting', [App\Http\Controllers\WebsiteSettingController::class, 'index']);

    /* Website Services */
    Route::get('/website/marketplaces', [App\Http\Controllers\WebsiteMarketplaceController::class, 'index']);
    Route::get('/website/marketplace/add/{slack?}', [App\Http\Controllers\WebsiteMarketplaceController::class, 'add']);
    
    /* Reports */
    Route::get('/report/engagement_report', [App\Http\Controllers\ReportController::class, 'engagement_report']);
    Route::get('/report/revenue_report', [App\Http\Controllers\ReportController::class, 'revenue_report'])->name('revenue_report');
    Route::get('/report/registration_report', [App\Http\Controllers\ReportController::class, 'registration_report'])->name('registration_report');

    /* Inquiry */
    Route::get('/inquiry/whatsapp_visits', [App\Http\Controllers\InquiryController::class, 'whatsapp_visits']);
    Route::get('/inquiry/demo_request', [App\Http\Controllers\InquiryController::class, 'demo_request']);
    
});
