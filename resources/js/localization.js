"use strict";

import Vue from 'vue';
import VueI18n from 'vue-i18n';
// import {i18n} from './plugins/i18n'

Vue.use(VueI18n);

/* import available lang files to vue */
export const language = window.settings.language;

import en from '../../resources/lang/en.json';
import ar from '../../resources/lang/ar.json';

/* import end */

export var messages = {
    'en' : en,
    'ar' : ar
}

// Create VueI18n instance with options
export const i18n = new VueI18n({
    locale: language, // set locale
    messages, // set locale messages
    silentTranslationWarn : true,
}); 

