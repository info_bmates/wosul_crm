import Vue from 'vue';
import Vuex from 'vuex';
// store modules
import auth from './modules/auth'; // authentication 

Vue.use(Vuex);

const store = new Vuex.Store({
    
    state : {},
    
    modules: { auth },

    getters: {},

    mutations: {},

    actions: {}

});

export default store;