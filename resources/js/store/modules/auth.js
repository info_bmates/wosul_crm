import axios from 'axios';

const state = {
  user : {},
  is_logged_in : false,
  menus : []
};

const getters = {

    loggedUserIsAdminText( state ){
      return (state.user.is_admin == 1) ? 'Admin' : 'User';
    }

};

const mutations = {
  SET_USER( state, data ){
    state.user = data;
  },
  SET_USER_MENUS( state, data ){
    state.menus = data;
  },
  SET_USER_IS_LOGGED_IN( state, data ){
    state.is_logged_in = data;
  },
};

const actions = {
  setUser( {commit} ){
    axios.get('/get/user').then( response => {
      commit('SET_USER',response.data);
      if(response.data){
        commit('SET_USER_IS_LOGGED_IN',true);
      }else{
        commit('SET_USER_IS_LOGGED_IN',false);
      }
    });
  },
  setUserMenu( {commit} ){
    axios.get('/user/menus').then( response => {
      commit('SET_USER_MENUS',response.data.data);
    });
  },
};

export default {
  namespaced : true,
  state ,
  getters,
  mutations,
  actions
}