/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue').default;
var _ = require('lodash');
 
import Vue from 'vue';
import store from './store';
import Vuelidate from 'vuelidate';
import {mixin} from './mixin';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import { i18n, language, messages } from './localization.js';
import 'vue-preloaders/dist/vue-preloaders.css';
import VuePreloaders from 'vue-preloaders';

Vue.use(store);
Vue.use(Vuelidate);
Vue.use(mixin);
Vue.use(VueSweetalert2);
Vue.use(VuePreloaders, /*{ options }*/)

axios.defaults.headers.common = {
    "Authorization": 'Bearer '+localStorage.getItem('access_token')
};

/* Layouts */
Vue.component('sidebar-layout-header', require('./components/layouts/sidebar/Header.vue').default);
Vue.component('sidebar-layout-sidebar', require('./components/layouts/sidebar/Sidebar.vue').default);
Vue.component('sidebar-layout-footer', require('./components/layouts/sidebar/Footer.vue').default);

/* Components */

// Auth
Vue.component('login', require('./components/Login.vue').default);

// Dashboard
Vue.component('dashboard', require('./components/Dashboard.vue').default);

// Role 
Vue.component('add-role', require('./components/role/Add.vue').default);
Vue.component('list-role', require('./components/role/List.vue').default);

// User
Vue.component('add-user', require('./components/user/Add.vue').default);
Vue.component('list-user', require('./components/user/List.vue').default);

// Merchant
Vue.component('list-merchant', require('./components/merchant/List.vue').default);
Vue.component('show-merchant', require('./components/merchant/Show.vue').default);

// Merchant
Vue.component('list-device', require('./components/device/List.vue').default);
Vue.component('add-device', require('./components/device/Add.vue').default);

Vue.component('list-subscription', require('./components/subscription/List.vue').default);
Vue.component('add-subscription', require('./components/subscription/Add.vue').default);
Vue.component('show-subscription', require('./components/subscription/Show.vue').default);
Vue.component('subscription-menu', require('./components/subscription/Menu.vue').default);

// Website Service 
Vue.component('list-website-service', require('./components/website/service/List.vue').default);
Vue.component('add-website-service', require('./components/website/service/Add.vue').default);


// Website Feature 
Vue.component('list-website-feature', require('./components/website/feature/List.vue').default);
Vue.component('add-website-feature', require('./components/website/feature/Add.vue').default);

// Website Review 
Vue.component('list-website-review', require('./components/website/review/List.vue').default);
Vue.component('add-website-review', require('./components/website/review/Add.vue').default);

// Website Gallery 
Vue.component('list-website-gallery', require('./components/website/gallery/List.vue').default);
Vue.component('add-website-gallery', require('./components/website/gallery/Add.vue').default);

// Website Client 
Vue.component('list-website-client', require('./components/website/client/List.vue').default);
Vue.component('add-website-client', require('./components/website/client/Add.vue').default);

// Website Partner 
Vue.component('list-website-partner', require('./components/website/partner/List.vue').default);
Vue.component('add-website-partner', require('./components/website/partner/Add.vue').default);

// Website Setting 
Vue.component('website-setting', require('./components/website/Setting.vue').default);

// Website Marketplace 
Vue.component('list-website-marketplace', require('./components/website/marketplace/List.vue').default);
Vue.component('add-website-marketplace', require('./components/website/marketplace/Add.vue').default);

// Website Marketplace 
Vue.component('list-whatsapp-visit', require('./components/inquiry/WhatsappVisitList.vue').default);
Vue.component('list-demo-request', require('./components/inquiry/DemoRequestList.vue').default);

// Reports
Vue.component('engagement-report', require('./components/report/EngagementReport.vue').default);
Vue.component('revenue-report', require('./components/report/RevenueReport.vue').default);
Vue.component('registration-report', require('./components/report/RegistrationReport.vue').default);

/* Other Components */
Vue.component('paginations', require('laravel-vue-pagination'));

const app = new Vue({
    store,
    el: '#app',
    i18n
});

if(localStorage.getItem('access_token')){
    console.log('access token found');
    store.dispatch("auth/setUser");
    store.dispatch("auth/setUserMenu");
}
