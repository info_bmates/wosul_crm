<div>
    @if ($status == 1)
        <div class="badge rounded-pill bg-success">Active</div>
    @elseif($status == 0)
        <div class="badge rounded-pill bg-danger">Inactive</div>
    @endif
</div>
