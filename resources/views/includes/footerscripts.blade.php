<script type="text/javascript">
    var hostUrl = "assets/";
</script>

<script type="text/javascript" src="{{ URL::asset('assets/plugins/global/plugins.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script type="text/javascript" src="{{ URL::asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}">
</script>
<script type="text/javascript" src="{{ URL::asset('assets/plugins/custom/datatables/datatables.bundle.js') }}">
</script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script type="text/javascript" src="{{ URL::asset('assets/js/widgets.bundle.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom/widgets.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom/apps/chat/chat.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom/utilities/modals/create-app.js') }}">
</script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom/utilities/modals/create-campaign.js') }}">
</script>
<script type="text/javascript" src="{{ URL::asset('assets/js/custom/utilities/modals/users-search.js') }}">
</script>
<script src="{{ mix('js/app.js') }}"></script>
