@extends('app')

@section('content')
    <add-role :edit_data="{{ json_encode($data['edit_data']) }}" :menus="{{ json_encode($data['menus']) }}"></add-role>
@endsection
