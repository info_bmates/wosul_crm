@extends('app')

@section('content')
    <subscription-menu :title="{{ json_encode($data['title']) }}" :edit_data="{{ json_encode($data['edit_data']) }}"
        :merchant_menus="{{ json_encode($data['merchant_menus']) }}">
    </subscription-menu>
@endsection
