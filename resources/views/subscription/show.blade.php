@extends('app')

@section('content')
    <show-subscription :subscription="{{ json_encode($data['subscription']) }}">
    </show-subscription>
@endsection
