@extends('app')

@section('content')
    <add-subscription :edit_data="{{ json_encode($data['edit_data']) }}">
    </add-subscription>
@endsection
