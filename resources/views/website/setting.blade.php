@extends('app')

@section('content')
    <website-setting :settings="{{ json_encode($data['settings']) }}">
    </website-setting>
@endsection
