@extends('app')

@section('content')
    <engagement-report :data="{{ json_encode($data) }}">
    </engagement-report>
@endsection
