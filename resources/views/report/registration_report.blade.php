@extends('app')

@section('content')
    <registration-report :data="{{ json_encode($data) }}">
    </registration-report>
@endsection
