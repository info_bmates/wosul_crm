@extends('app')

@section('content')
    <revenue-report :data="{{ json_encode($data) }}">
    </revenue-report>
@endsection
