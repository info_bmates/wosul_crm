@extends('app')

@section('content')
    <show-merchant :merchant="{{ json_encode($data['merchant']) }}">
    </show-merchant>
@endsection
