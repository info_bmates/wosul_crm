{{-- label with info --}}

<label class="fs-6 fw-bold form-label mt-3">
    <span class="required">Name</span>
    <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title=""
        data-bs-original-title="Enter the contact's name." aria-label="Enter the contact's name."></i>
</label>

{{-- simple label --}}

<label class="fs-6 fw-bold form-label mt-3">
    <span class="required">Name</span>
</label>


{{-- action button group --}}

<a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click"
    data-kt-menu-placement="bottom-end">Actions
    <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
    <span class="svg-icon svg-icon-5 m-0">
        <i class="fa fa-chevron-down fs-1x px-1"></i>
    </span>
    <!--end::Svg Icon-->
</a>
<!--begin::Menu-->
<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
    data-kt-menu="true" style="">
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="/metronic8/demo14/../demo14/apps/subscriptions/add.html" class="menu-link px-3">View</a>
    </div>
    <!--end::Menu item-->
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="/metronic8/demo14/../demo14/apps/subscriptions/add.html" class="menu-link px-3">Edit</a>
    </div>
    <!--end::Menu item-->
    <!--begin::Menu item-->
    <div class="menu-item px-3">
        <a href="#" data-kt-subscriptions-table-filter="delete_row" class="menu-link px-3">Delete</a>
    </div>
    <!--end::Menu item-->
</div>
<!--end::Menu-->
