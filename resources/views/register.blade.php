<!DOCTYPE html>
<html>

<head>

    @include('includes.headerscripts')

    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>

    <div id="app">

        <register></register>

    </div>

    @include('includes/footerscripts')

</body>

</html>
