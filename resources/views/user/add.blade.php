@extends('app')

@section('content')
    <add-user :edit_data="{{ json_encode($data['edit_data']) }}" :roles="{{ json_encode($data['roles']) }}"></add-user>
@endsection
