@extends('app')

@section('content')
    <list-user :roles="{{ json_encode($data['roles']) }}"></list-user>
@endsection
