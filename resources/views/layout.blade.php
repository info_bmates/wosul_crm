<!DOCTYPE html>
<html>

<head>
    @include('includes.headerscripts')

    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<div id="app">

    <body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed ">
        <!--begin::Main-->
        <!--begin::Root-->
        <div class="d-flex flex-column flex-root">
            <!--begin::Page-->
            <div class="page d-flex flex-row flex-column-fluid">
                <!--begin::Wrapper-->
                <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                    <sidebar-layout-header></sidebar-layout-header>
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column-fluid">

                        <sidebar-layout-sidebar></sidebar-layout-sidebar>

                        <!--begin::Container-->
                        <div class="d-flex flex-column flex-column-fluid container-fluid">
                            <!--begin::Post-->
                            <div class="content flex-column-fluid" id="kt_content">
                                <!-- Content comes here -->
                                @yield('content')
                            </div>
                            <!--end::Post-->
                            <sidebar-layout-footer></sidebar-layout-footer>
                        </div>
                        <!--end::Container-->
                    </div>
                    <!--end::Content wrapper-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Page-->
        </div>

        @include('includes.footerscripts')

    </body>
</div>

</html>
