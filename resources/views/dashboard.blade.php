@extends('app')

@section('content')
    <dashboard :stats="{{ json_encode($data['stats']) }}" :chart="{{ json_encode($data['chart']) }}"></dashboard>
@endsection
