<?php

namespace App\Exports;

use App\Http\Resources\MerchantResource;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Str;

class RegistrationExport implements FromArray,WithHeadings,WithStyles,WithMapping
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'Merchant',
            'Company',
            'Email',
            'Status',
            'Registered At',
            'Link'
        ];
    }

    public function map($merchants): array{

        $data = [];
        foreach($merchants->get() as $merchant){
            
            $merchant = new MerchantResource($merchant);
            $merchant = $merchant->toArray($merchant);
            
            $data[] =  [
                'merchant' => $merchant['name'],
                'company' => Str::lower($merchant['company_url']),
                'email' => $merchant['email'],
                'status' => $merchant['status_text'],
                'registered_at' => $merchant['registered_at'],
                'merchant_url' => $merchant['merchant_url']
            ];

        }

        return $data;

    }
    

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]]
        ];
    }

}
