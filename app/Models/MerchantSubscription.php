<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class MerchantSubscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'subscription_id',
        'merchant_id',
        'start_date',
        'end_date',
        'status',
        'deleted_at',
        'updated_at',
        'created_at'
    ];

    public $timestamps = true;

    /* Scopes */

    public function scopeActive($query)
    {
        return $query->where('merchant_subscriptions.status', 1);
    }

    /* Relations */

    public function subscription()
    {
        return $this->belongsTo('App\Models\Subscription')->select(['slack', 'title','title_ar']);
    }
    
    public function createdUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function updatedUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }

}
