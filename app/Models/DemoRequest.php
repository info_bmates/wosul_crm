<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class DemoRequest extends Model
{
    use HasFactory;

    public function scopeSearch($query, $filters)
    {
        $filters = json_decode($filters, true);

        if ($filters['search_term'] != '') {

            $search_term = '%' . $filters['search_term'] . '%';

            $query->where(function ($query) use ($search_term) {
                $query->where('first_name', 'like', trim($search_term));
                $query->orWhere('last_name', 'like', trim($search_term));
                $query->orWhere('city', 'like', trim($search_term));
                $query->orWhere('email', 'like', trim($search_term));
                    // ->orWhere('role_code', 'like', trim($search_term));
            });
        }
        
        if ($filters['status'] != '') {
            $query->where('status', $filters['status']);
        }

    }

    public function getFullNameAttribute(){
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }
    
    public function getStatusTextAttribute(){
        if($this->attributes['status'] == 0){
            return 'Open';
        }else if($this->attributes['status'] == 1){
            return 'Under Processing';
        }else if($this->attributes['status'] == 2){
            return 'Closed';
        }
    }


    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
