<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MerchantMenu extends Model
{

    public function scopeActive($query)
    {
        return $query->whereStatus(1);
    }

    public function scopeMainMenuOnly($query)
    {
        return $query->whereType('MAIN_MENU');
    }

    public function scopeSubMenuOnly($query)
    {
        return $query->whereType('SUB_MENU');
    }

    public function scopeActionOnly($query)
    {
        return $query->whereType('ACTION');
    }

    public function submenus()
    {
        return $this->hasMany('App\Models\MeMerchantMenunu', 'parent')->with('submenus');
    }

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
