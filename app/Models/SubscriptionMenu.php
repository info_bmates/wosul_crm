<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionMenu extends Model
{
    protected $hidden = ['id'];
    protected $fillable = ['subscription_id', 'menu_id', 'created_by', 'updated_by'];

    /* Relations */

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }

    public function subscription()
    {
        return $this->belongsTo('App\Models\Subscription', 'id', 'subscription_id');
    }
}
