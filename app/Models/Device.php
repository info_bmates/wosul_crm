<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Device extends Model
{
    use HasFactory;

    protected $fillable = [
        'slack',
        'image',
        'title',
        'title_ar',
        'short_description',
        'short_description_ar',
        'currency',
        'amount',
        'discount',
        'discount_description',
        'discount_description_ar',
        'is_featured',
        'status',
        'created_by',
        'updated_by',
    ];

    public $timestamps = true;

    /* Scopes */

    public function scopeActive($query)
    {
        return $query->where('devices.status', 1);
    }

    public function scopeSearch($query, $filters)
    {
        $filters = json_decode($filters, true);

        if ($filters['search_term'] != '') {

            $search_term = '%' . $filters['search_term'] . '%';

            $query->where(function ($query) use ($search_term) {
                $query->where('title', 'like', trim($search_term))
                    ->orWhere('title_ar', 'like', trim($search_term))
                    ->orWhere('short_description', 'like', trim($search_term))
                    ->orWhere('short_description_ar', 'like', trim($search_term))
                    ->orWhere('amount', 'like', trim($search_term));
            });
        }

        if ($filters['status'] != '') {
            $query->where('status', $filters['status']);
        }
    }

    /* Accessors & Mutators */

    public function getStatusTextAttribute()
    {
        return ($this->status == 1) ? 'Active' : 'Inactive';
    }

    public function getImagePathAttribute()
    {
        return Storage::disk('device')->url($this->attributes['image']);
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = ($value == 'true') ? 1 : 0;
    }

    public function setIsFeaturedAttribute($value)
    {
        $this->attributes['is_featured'] = ($value == 'true') ? 1 : 0;
    }

    /* Joins */
    public function scopeCreatedUser($query)
    {
        return $query->leftJoin('users AS user_created', function ($join) {
            $join->on('user_created.id', '=', 'devices.created_by');
        });
    }

    public function scopeUpdatedUser($query)
    {
        return $query->leftJoin('users AS user_updated', function ($join) {
            $join->on('user_created.id', '=', 'devices.updated_by');
        });
    }

    /* Relations */

    public function createdUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function updatedUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
