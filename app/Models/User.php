<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasApiTokens, SoftDeletes, HasFactory;

    protected $guard = "user";

    protected $fillable = [
        'slack',
        'name',
        'email',
        'password',
        'phone',
        'role_id',
        'status',
        'is_admin',
        'created_by',
        'updated_by'
    ];

    public $timestamps = true;

    protected $hidden = [
        'password'
    ];

    /* Scopes */

    public function scopeActive($query)
    {
        return $query->where('users.status', 1);
    }

    public function scopeExceptAdminUser($query)
    {
        return $query->where('users.role_id', '!=', 1);
    }

    public function scopeSearch($query, $filters)
    {
        $filters = json_decode($filters, true);

        if ($filters['search_term'] != '') {

            $search_term = '%' . $filters['search_term'] . '%';

            $query->where(function ($query) use ($search_term) {
                $query->where('name', 'like', trim($search_term))
                    ->orWhere('email', 'like', trim($search_term))
                    ->orWhere('phone', 'like', trim($search_term));
            });
        }

        if ($filters['role'] != '') {
            $query->where('role_id', $filters['role']);
        }

        if ($filters['status'] != '') {
            $query->where('status', $filters['status']);
        }
    }

    /* Accessors & Mutators */

    public function getStatusTextAttribute()
    {
        return ($this->status == 1) ? 'Active' : 'Inactive';
    }

    public function getIsAdminTextAttribute()
    {
        return ($this->is_admin == 1) ? 'Admin' : 'User';
    }

    public function getIsEmailVerifiedTextAttribute()
    {
        return ($this->is_email_verified == 1) ? 'Active' : 'Inactive';
    }

    public function getIsPhoneVerifiedTextAttribute()
    {
        return ($this->is_phone_verified == 1) ? 'Active' : 'Inactive';
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = ($value == 'true') ? 1 : 0;
    }

    public function setDarkModeAttribute($value)
    {
        $this->attributes['dark_mode'] = ($value == 'true') ? 1 : 0;
    }

    /* Relations */

    public function role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }

    public function createdUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function updatedUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
