<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Merchant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'slack',
        'subscription_id',
        'name',
        'phone_number',
        'email',
        'password',
        'company_name',
        'company_url',
        'address',
        'referral_code',
        'recommendation',
        'status',
        'created_by',
        'updated_by'
    ];

    public $timestamps = true;

    protected $hidden = [
        'password'
    ];

    /* Scopes */

    public function scopeActive($query)
    {
        return $query->where('merchants.status', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('merchants.status', '!=', 1);
    }

    public function scopeSearch($query, $filters)
    {
        $filters = json_decode($filters, true);

        if ($filters['search_term'] != '') {

            $search_term = '%' . $filters['search_term'] . '%';

            $query->where(function ($query) use ($search_term) {
                $query->where('name', 'like', trim($search_term))
                    ->orWhere('email', 'like', trim($search_term))
                    ->orWhere('phone_number', 'like', trim($search_term))
                    ->orWhere('company_name', 'like', trim($search_term))
                    ->orWhere('company_url', 'like', trim($search_term))
                    ->orWhere('address', 'like', trim($search_term));
            });
        }

        if ($filters['status'] != '') {
            $query->where('status', $filters['status']);
        }
    }

    /* Accessors & Mutators */

    public function getStatusTextAttribute()
    {
        return ($this->status == 1) ? 'Active' : 'Inactive';
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = ($value == 'true') ? 1 : 0;
    }
    


    /* Relations */

    public function merchant_subscription(){
        return $this->hasOne('App\Models\MerchantSubscription','merchant_id','id');
    }
    
    public function createdUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function updatedUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by')->select(['slack', 'name', 'email', 'phone']);
    }

    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
