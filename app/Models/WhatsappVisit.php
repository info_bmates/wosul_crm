<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class WhatsappVisit extends Model
{
    use HasFactory;

    protected $table = "whatsapp_page_visit";


    public function scopeSearch($query, $filters)
    {
        $filters = json_decode($filters, true);

        if ($filters['search_term'] != '') {

            $search_term = '%' . $filters['search_term'] . '%';

            $query->where(function ($query) use ($search_term) {
                $query->where('ip', 'like', trim($search_term));
                    // ->orWhere('role_code', 'like', trim($search_term));
            });
        }

    }


    public function parseDate($date)
    {
        return ($date != null) ? Carbon::parse($date)->format(config("constants.date_time_format")) : null;
    }
}
