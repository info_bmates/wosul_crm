<?php

use App\Models\Menu;

if (!function_exists('getMenus')) {
    function getMenus()
    {

        $menu_array = [];
        $menu_tree = [];


        if (session('logged_user_id') == 1) {
            $main_menus = DB::table('menus')
                ->select('id', 'label', 'type')
                ->orderBy('sort_order', 'ASC')
                ->where('type', '=', 'MAIN_MENU')
                ->get();
        } else {

            $main_menus = DB::table('menus')
                ->join('user_menus', 'user_menus.menu_id', 'menus.id')
                ->select('menus.id', 'menus.label', 'menus.type')
                ->orderBy('sort_order', 'ASC')
                ->where('status', '=', 1)
                ->where('user_menus.user_id', session('logged_user_id'))
                ->where('type', '=', 'MAIN_MENU')
                ->get();
        }

        foreach ($main_menus as $main_menu) {

            $menu_array['_' . $main_menu->id] = [
                "menu_key" => $main_menu->id,
                "label" => $main_menu->label,
                "type" => $main_menu->type,
            ];

            if (session('logged_user_id') == 1) {
                $sub_menus = DB::table('menus')
                    ->orderBy('sort_order', 'ASC')
                    ->where('status', '=', 1)
                    ->where('parent', '=', $main_menu->id)
                    ->get();
            } else {
                $sub_menus = DB::table('menus')
                    ->join('user_menus', 'user_menus.menu_id', 'menus.id')
                    ->select('menus.id', 'menus.label', 'menus.type')
                    ->orderBy('sort_order', 'ASC')
                    ->where('status', '=', 1)
                    //->where('type', '=', 'SUB_MENU')
                    ->where('parent', '=', $main_menu->id)
                    ->where('user_menus.user_id', session('logged_user_id'))
                    ->get();
            }


            $all_sub_menu_ids = array_pluck($sub_menus, 'id');
            $menu_array['_' . $main_menu->id]['childs'] = $all_sub_menu_ids;

            foreach ($sub_menus as $sub_menu) {

                $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id] = [
                    "menu_key" => $sub_menu->id,
                    "label" => $sub_menu->label,
                    "type" => $sub_menu->type,
                    "sub_items" => [$sub_menu->id]
                ];
                $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['siblings'] = $all_sub_menu_ids;

                if ($sub_menu->type == 'SUB_MENU') {

                    if (session('logged_user_id') == 1) {
                        $action_menus = DB::table('menus')
                            ->select('menus.id', 'menus.label', 'menus.type')
                            ->orderBy('sort_order', 'ASC')
                            ->where('status', '=', 1)
                            ->where('type', '=', 'ACTIONS')
                            ->where('parent', '=', $sub_menu->id)
                            ->get();
                    } else {
                        $action_menus = DB::table('menus')
                            ->join('user_menus', 'user_menus.menu_id', 'menus.id')
                            ->select('menus.id', 'menus.label', 'menus.type')
                            ->orderBy('sort_order', 'ASC')
                            ->where('status', '=', 1)
                            ->where('type', '=', 'ACTIONS')
                            ->where('parent', '=', $sub_menu->id)
                            ->where('user_menus.user_id', session('logged_user_id'))
                            ->get();
                    }


                    $all_action_menu_ids = array_pluck($action_menus, 'id');
                    $menu_array['_' . $main_menu->id]['childs'] = array_merge($menu_array['_' . $main_menu->id]['childs'], $all_action_menu_ids);
                    $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['childs'] = $all_action_menu_ids;

                    foreach ($action_menus as $action_menu) {
                        if (!in_array($action_menu->label, ["Delete Order", "Delete Invoice", "Edit Invoice", "Delete Business Register"])) {
                            $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['actions'][] = [
                                "menu_key" => $action_menu->id,
                                "label" => $action_menu->label,
                                "type" => $action_menu->type,
                                "siblings" => $all_action_menu_ids
                            ];
                        }
                    }
                }
            }
        }

        return $menu_array;
    }
}

if (!function_exists('getMerchantMenus')) {
    function getMerchantMenus($subscription_id)
    {

        $menu_array = [];
        $menu_tree = [];

        $main_menus = DB::table('merchant_menus')
            ->select('id', 'label', 'type')
            ->orderBy('sort_order', 'ASC')
            ->where('type', '=', 'MAIN_MENU')
            ->get();

        foreach ($main_menus as $main_menu) {

            $menu_array['_' . $main_menu->id] = [
                "menu_key" => $main_menu->id,
                "label" => $main_menu->label,
                "type" => $main_menu->type,
            ];

            $sub_menus = DB::table('merchant_menus')
                ->select('id', 'label', 'type')
                ->orderBy('sort_order', 'ASC')
                ->where('type', 'SUB_MENU')
                ->where('parent', $main_menu->id)
                ->get();

            $all_sub_menu_ids = array_pluck($sub_menus, 'id');
            $menu_array['_' . $main_menu->id]['childs'] = $all_sub_menu_ids;

            foreach ($sub_menus as $sub_menu) {

                $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id] = [
                    "menu_key" => $sub_menu->id,
                    "label" => $sub_menu->label,
                    "type" => $sub_menu->type,
                    "sub_items" => [$sub_menu->id]
                ];
                $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['siblings'] = $all_sub_menu_ids;

                if ($sub_menu->type == 'SUB_MENU') {

                    $action_menus = DB::table('merchant_menus')
                        ->select('id', 'label', 'type')
                        ->orderBy('sort_order', 'ASC')
                        ->where('status', '=', 1)
                        ->where('type', 'ACTIONS')
                        ->where('parent', $sub_menu->id)
                        ->get();

                    $all_action_menu_ids = array_pluck($action_menus, 'id');
                    $menu_array['_' . $main_menu->id]['childs'] = array_merge($menu_array['_' . $main_menu->id]['childs'], $all_action_menu_ids);
                    $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['childs'] = $all_action_menu_ids;

                    foreach ($action_menus as $action_menu) {
                        if (!in_array($action_menu->label, ["Delete Order", "Delete Invoice", "Edit Invoice", "Delete Business Register"])) {
                            $menu_array['_' . $main_menu->id]['sub_menu'][$sub_menu->id]['actions'][] = [
                                "menu_key" => $action_menu->id,
                                "label" => $action_menu->label,
                                "type" => $action_menu->type,
                                "siblings" => $all_action_menu_ids
                            ];
                        }
                    }
                }
            }
        }

        return $menu_array;
    }
}

if (!function_exists('check_access')) {
    function check_access($menu_array, $return = false)
    {
        $access_check_array = [];

        $logged_user_role_id = session('logged_user_role_id');

        //For super admin by default allow all the actions and menus
        if ($logged_user_role_id != 1) {
            $menu_ids = Menu::select('id')->active()->whereIn('menu_key', $menu_array)->get()->pluck('id');
            if (count($menu_ids) != count($menu_array)) {
                $access_check_array[] = false;
            }

            $logged_user_menus_access = session('logged_user_menus');

            if (count($menu_ids) > 0) {
                foreach ($menu_ids as $menu_array_item) {
                    if (is_array($logged_user_menus_access) && !in_array($menu_array_item, $logged_user_menus_access)) {
                        $access_check_array[] = false;
                    } else {
                        $access_check_array[] = true;
                    }
                }
            } else {
                $access_check_array[] = false;
            }
        } else {
            $access_check_array[] = true;
        }

        $access_check_result = (in_array(false, $access_check_array)) ? false : true;

        if ($return == false) {
            ($access_check_result == false) ? abort(403) : $access_check_result;
        } else {
            return $access_check_result;
        }
    }
}
