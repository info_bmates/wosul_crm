<?php

if (!function_exists('createConnection')) {
    function createConnection($db)
    {
        return new MysqliDb('localhost', env('DB_USERNAME'), env('DB_PASSWORD'), $db);
    }
}

if (!function_exists('closeConnection')) {
    function closeConnection($connection)
    {
        $connection->disconnect();
    }
}

// if (!function_exists('getOne')) {
//     function getOne($connection, $table, $field, $value)
//     {
//         $data = [];
//         $query = "SELECT * FROM  " . $table . " WHERE " . $field . " = '" . $value . "' LIMIT 1 ";
//         $query = mysqli_query($connection, $query);
//         if (mysqli_num_rows($query) > 0) {
//             $data = mysqli_fetch_assoc($query);
//         }

//         return $data;
//     }
// }

// if (!function_exists('getRaw')) {
//     function getRaw($connection, $table, $field, $value)
//     {
//         $data = [];
//         $query = "SELECT * FROM  " . $table . " WHERE " . $field . " = '" . $value . "' ";
//         $query = mysqli_query($connection, $query);

//         $data = [];
//         if (mysqli_num_rows($query) > 0) {
//             while ($row = mysqli_fetch_assoc($query)) {
//                 $data[] = $row;
//             }
//         }
//         return $data;
//     }
// }

// if (!function_exists('getCount')) {
//     function getCount($connection, $table, $field, $value)
//     {
//         $data = [];
//         $query = "SELECT * FROM  " . $table . " WHERE " . $field . " = '" . $value . "' LIMIT 1 ";
//         $query = mysqli_query($connection, $query);
//         if (mysqli_num_rows($query) > 0) {
//             $data = mysqli_fetch_assoc($query);
//         }

//         return $data;
//     }
// }

if (!function_exists('getMerchantLogo')) {
    function getMerchantLogo($db, $merchant_id, $merchant_url)
    {
        $logo = $db->where('id', 1)->getOne('stores', 'store_logo');
        if ($logo['store_logo']  != '') {
            return $merchant_url . '/storage/' . $merchant_id . '/store/' . $logo['store_logo'];
        } else {
            return $merchant_url . '/public/images/logo.png';
        }
    }
}

if (!function_exists('getStoreLogo')) {
    function getStoreLogo($db, $merchant_id, $merchant_url, $store_id)
    {
        $logo = $db->where('id', $store_id)->getOne('stores', 'store_logo');
        if ($logo['store_logo']  != '') {
            return $merchant_url . '/storage/' . $merchant_id . '/store/' . $logo['store_logo'];
        } else {
            return $merchant_url . '/public/images/logo.png';
        }
    }
}
