<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function generate_response($response_array, $type = "")
    {
        switch ($type) {
            case "SUCCESS":
                $status_code = 200;
                break;
            case "NOT_AUTHORIZED":
                $status_code = 401;
                break;
            case "NO_ACCESS":
                $status_code = 403;
                break;
            case "BAD_REQUEST":
                $status_code = 400;
                break;
            default:
                $status_code = 200;
                break;
        }
        $response = array(
            'status' => true,
            'msg'    => (isset($response_array['message'])) ? $response_array['message'] : "",
            'data'   => (isset($response_array['data'])) ? $response_array['data'] : "",
            'status_code' => (isset($response_array['status_code'])) ? $response_array['status_code'] : $status_code
        );
        if (isset($response_array['link'])) {
            $response = array_merge($response, array("link" => $response_array['link']));
        }
        if (isset($response_array['pdf_link'])) {
            $response = array_merge($response, array("pdf_link" => $response_array['pdf_link']));
        }
        if (isset($response_array['new_tab'])) {
            $response = array_merge($response, array("new_tab" => $response_array['new_tab']));
        }
        if (isset($response_array['order'])) {
            $response = array_merge($response, array("order" => $response_array['order']));
        }
        if (isset($response_array['type'])) {
            $response = array_merge($response, array("type" => $response_array['type']));
        }

        return $response;
    }

    function generate_slack($table)
    {
        do {
            $slack = Str::random(10);
            $exist = DB::table($table)->where("slack", $slack)->first();
        } while ($exist);
        return $slack;
    }

    // for listing table apis
    public function no_access_response_for_listing_table()
    {
        $response = [
            'draw' => 0,
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
            'data' => [],
            'access' => false
        ];
        return response()->json($response);
    }
}
