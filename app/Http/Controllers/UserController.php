<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{

    use AuthenticatesUsers;

    public function index()
    {
        check_access(['MM_USER_AND_ROLE', 'SM_USERS']);

        $data['title'] = 'Users';

        $roles = RoleResource::collection(Role::exceptAdminRole()->active()->orderByLabel()->get());
        $data['roles'] = $roles->toArray($roles);

        return view('user.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add User';
        $data['edit_data'] = null;
        $data['roles'] = RoleResource::collection(Role::exceptAdminRole()->orderByLabel()->get());

        if ($slack != null) {
            check_access(['MM_USER_AND_ROLE', 'SM_USERS', 'A_EDIT_USER']);
            $data['title'] = 'Edit User';
            $user = User::where('slack', $slack)->first();
            $data['edit_data'] = new UserResource($user);
        } else {
            check_access(['MM_USER_AND_ROLE', 'SM_USERS', 'A_ADD_USER']);
        }

        return view('user.add', compact('data'));
    }

    public function getUserMenus()
    {
        $menus =  Menu::select('menus.*')
            ->join('user_menus', 'user_menus.menu_id', 'menus.id')
            ->where('user_menus.user_id', request()->user()->id)
            ->active()
            ->mainMenuOnly()
            ->orderBy('id', 'ASC')
            ->get();
        return MenuResource::collection($menus);
    }
}
