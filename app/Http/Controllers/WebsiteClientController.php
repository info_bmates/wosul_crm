<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteClient;
use App\Http\Resources\WebsiteClientResource;

class WebsiteClientController extends Controller
{

    public function index()
    {
        $data['title'] = 'Client';

        // get all clients
        $clients = WebsiteClient::all();
        $data['clients'] = WebsiteClientResource::collection($clients);

        return view('website.client.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Client';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit client';
            $data['edit_data'] = WebsiteClient::where('slack', $slack)->first();
        }
        return view('website.client.add', compact('data'));
    }
}
