<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteGallery;
use App\Http\Resources\WebsiteGalleryResource;

class WebsiteGalleryController extends Controller
{

    public function index()
    {
        $data['title'] = 'Gallery';

        // get all gallery
        $gallery = WebsiteGallery::all();
        $data['gallery'] = WebsiteGalleryResource::collection($gallery);

        return view('website.gallery.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Gallery';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit gallery';
            $data['edit_data'] = WebsiteGallery::where('slack', $slack)->first();
        }
        return view('website.gallery.add', compact('data'));
    }
}
