<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class MerchantController extends Controller
{

    use AuthenticatesUsers;

    public function index()
    {
        check_access(['MM_MERCHANT']);
        $data['title'] = 'Merchants';
        return view('merchant.list', compact('data'));
    }

    public function show($slack)
    {
        check_access(['MM_MERCHANT']);
        $data['title'] = 'Merchant Detail';

        $merchant =  new MerchantResource(Merchant::where('slack', $slack)->first());
        $data['merchant'] = $merchant->toArray($merchant);

        /* Getting the data from merchant database : start */
        $db = createConnection($data['merchant']['db_name']);

        $data['merchant']['total_orders'] = $db->getValue("orders", "count(*)");
        $data['merchant']['total_users'] = $db->getValue("users", "count(*)");
        $data['merchant']['total_stores'] = $db->getValue("stores", "count(*)");
        $data['merchant']['logo'] = getMerchantLogo($db, $data['merchant']['id'], $data['merchant']['company_url_full']);

        $data['merchant']['store_data'] = [];
        $merchant_stores = $db->get('stores', null, ['id', 'slack', 'store_logo', 'name', 'status', 'created_at']);
        if (isset($merchant_stores) && count($merchant_stores) > 0) {
            foreach ($merchant_stores as $row) {
                $dataset = [];
                $dataset['slack'] = $row['slack'];
                $dataset['logo'] = getStoreLogo($db, $data['merchant']['id'], $data['merchant']['company_url_full'], $row['id']);
                $dataset['store_name'] = $row['name'];
                $total_users = $db->where('store_id', $row['id'])->getOne('users', 'count(*) as count');
                $dataset['total_users'] =  $total_users['count'];
                $total_orders = $db->where('store_id', $row['id'])->getOne('orders', 'count(*) as count');
                $dataset['total_orders'] =  $total_orders['count'];
                $total_order_amount = $db->where('store_id', $row['id'])->getOne('orders', 'SUM(total_order_amount) as sum');
                $dataset['total_order_amount'] =  $total_order_amount['sum'];
                $dataset['status'] = $row['status'];
                $dataset['created_at'] = date('M d, Y', strtotime($row['created_at']));
                $data['merchant']['store_data'][] = $dataset;
            }
        }

        $data['merchant']['user_data'] = [];
        $merchant_users = $db->get('users', null, ['id', 'slack', 'fullname', 'email', 'phone', 'role_id', 'store_id', 'is_master', 'status', 'created_at']);
        if (isset($merchant_users) && count($merchant_users) > 0) {
            foreach ($merchant_users as $row) {
                $dataset = [];
                $dataset['slack'] = $row['slack'];
                $dataset['name'] = $row['fullname'];
                $dataset['email'] = $row['email'];
                $dataset['phone'] = $row['phone'];
                $user_role = $db->where('id', $row['role_id'])->getOne('roles', 'label');
                $dataset['role_name'] =  (isset($user_role)) ? $user_role['label'] : '';
                $user_store = $db->where('id', $row['store_id'])->getOne('stores', 'name');
                $dataset['store_name'] =  (isset($user_store)) ? $user_store['name'] : '';
                $dataset['is_master'] = $row['is_master'];
                $dataset['status'] = $row['status'];
                $dataset['created_at'] = date('M d, Y', strtotime($row['created_at']));
                $data['merchant']['user_data'][] = $dataset;
            }
        }

        closeConnection($db);
        /* Getting the data from merchant database : ends */

        return view('merchant.show', compact('data'));
    }
}
