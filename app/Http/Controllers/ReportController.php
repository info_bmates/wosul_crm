<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Merchant;

class ReportController extends Controller
{

    public function engagement_report()
    {
        $data['title'] = 'Enagement Report';

        return view('report.engagement_report', compact('data'));
    }
    
    public function revenue_report()
    {
        $data['title'] = 'Revenue Report';

        $data['years'] = range(Carbon::now()->year - 4, Carbon::now()->year);
        $data['months'] = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'Septemeber',
            'October',
            'Novemeber',
            'December'
        ];

        $data['merchants'] = Merchant::select('id','company_url')->active()->get();

        return view('report.revenue_report', compact('data'));
    }
    
    public function registration_report()
    {
        $data['title'] = 'Registration Report';

        return view('report.registration_report', compact('data'));
    }
}
