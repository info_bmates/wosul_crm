<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Models\User;
use App\Models\Role;
use App\Models\Subscription;
use App\Models\Device;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index()
    {
        $data['title'] = 'Dashboard';

        $data['stats'] = [];
        $data['stats']['total_merchants'] = Merchant::all()->count();
        // $data['stats']['total_active_merchants'] = Merchant::active()->get()->count();
        $data['stats']['total_merchants_this_month'] = Merchant::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->get()->count();
        $data['stats']['total_merchants_last_month'] = Merchant::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->subMonth()->format('m'))->get()->count();

        $data['stats']['total_users'] = User::all()->count();
        $data['stats']['total_roles'] = Role::all()->count();
        $data['stats']['total_subscriptions'] = Subscription::all()->count();
        $data['stats']['total_devices'] = Device::all()->count();

        $data['chart']['merchant_growth_chart'] = [];
        $merchant_growth_chart = DB::select('SELECT YEAR(created_at) as year,MONTH(created_at) as month,COUNT(*) as count FROM merchants GROUP BY YEAR(created_at),MONTH(created_at) ORDER BY YEAR(created_at)');
        $merchant_growth_chart = array_reverse(array_slice(array_reverse($merchant_growth_chart), 0, 6));

        foreach ($merchant_growth_chart as $chart) {
            $data['chart']['merchant_growth_chart']['categories'][] = Carbon::create()->day(1)->month($chart->month)->format('M') . ', ' . $chart->year;
            $data['chart']['merchant_growth_chart']['data'][] = $chart->count;
        }

        /* Getting the data from merchant database : start */
        
        $merchants = Merchant::select('company_url')->active()->whereNotIn('company_url',['demo','wosulsupport'])->get();

        $total_orders = 0;
        $total_order_amount = 0;
        $total_active_merchants = 0;
        
        if (isset($merchants) && count($merchants) > 0) {
            
            foreach ($merchants as $merchant) {
                /* Getting the data from merchant database : start */
                $merchant_dbname = Str::lower($merchant->company_url)."_wosul";
                // check if database exists or not
                $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
                $connect = DB::select($query, [$merchant_dbname]);
                if (!empty($connect)) {
                    
                    $total_active_merchants++;

                    $db = createConnection($merchant_dbname);
                    
                    $getTotalOrders = $db->getOne('orders', 'count(*) as count');
                    $total_orders +=  (double) $getTotalOrders['count'];
                    $getTotalOrderAmount = $db->getOne('orders', 'SUM(total_order_amount) as sum');
                    $total_order_amount +=  (double) $getTotalOrderAmount['sum'];

                    closeConnection($db);
                }
            }
            
        }

        $data['stats']['total_orders'] = number_format($total_orders,2);
        $data['stats']['total_order_amount'] = number_format($total_order_amount,2);
        $data['stats']['total_active_merchants'] = $total_active_merchants;

        // static stats 
        
        $data['chart']['merchant_growth_chart']['categories'] = ['Feb, 2022','Mar, 2022','Apr, 2022','May, 2022','Jun, 2022','Jul, 2022','Aug, 2022'];
        $data['chart']['merchant_growth_chart']['data'] = ['36','46','69','102','112','118','135'];

        
        return view('dashboard', compact('data'));
    }
}
