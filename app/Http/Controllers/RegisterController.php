<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\CountryMasterResource;
use App\Models\CountryMaster;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class RegisterController extends Controller
{

    use AuthenticatesUsers;

    public function index()
    {
        $data['title'] = 'Registration';
        return view('register', $data);
    }

    public function register(RegisterRequest $request)
    {
        dd($request);

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        try {

            $data = [];

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                $user = Auth::user();
                $token =  $user->createToken('MyApp')->accessToken;

                $data['access_token'] = $token;

                $data['redirect_url'] = route('dashboard');
            } else {

                throw new Exception("Invalid Username or Password");
            }


            return response()->json($this->generate_response(
                array(
                    "message" => "You are successfully logged in",
                    "data"    => $data
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {
            return response()->json($this->generate_response(
                [
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode(),
                ]
            ));
        }
    }

    public function logout()
    {

        Auth::logout();
        return redirect(route('login'));
    }
}
