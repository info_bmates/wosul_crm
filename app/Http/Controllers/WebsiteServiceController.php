<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteService;
use App\Http\Resources\WebsiteServiceResource;

class WebsiteServiceController extends Controller
{

    public function index()
    {
        $data['title'] = 'Devices';

        // get all services
        $services = WebsiteService::all();
        $data['services'] = WebsiteServiceResource::collection($services);

        return view('website.service.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Service';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit Service';
            $data['edit_data'] = WebsiteService::where('slack', $slack)->first();
        }
        return view('website.service.add', compact('data'));
    }
}
