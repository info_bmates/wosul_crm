<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use App\Models\SupportTicket;
use Illuminate\Support\Facades\DB;

class SalesAndSupportController extends Controller
{

    public function support_tickets()
    {
        $data['title'] = 'Support Tickets';

        return view('sales_and_support.support_ticket', compact('data'));
    }
    
    
}
