<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Device;
use App\Http\Resources\DeviceResource;

class DeviceController extends Controller
{

    public function index()
    {
        $data['title'] = 'Devices';

        // get all devices
        $devices = Device::all();
        $data['devices'] = DeviceResource::collection($devices);

        return view('device.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Device';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit Device';
            $data['edit_data'] = Device::where('slack', $slack)->first();
        }

        return view('device.add', compact('data'));
    }
}
