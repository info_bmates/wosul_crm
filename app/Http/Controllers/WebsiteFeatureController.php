<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteFeature;
use App\Http\Resources\WebsiteFeatureResource;

class WebsiteFeatureController extends Controller
{

    public function index()
    {
        $data['title'] = 'Devices';

        // get all features
        $features = WebsiteFeature::all();
        $data['features'] = WebsiteFeatureResource::collection($features);

        return view('website.feature.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Feature';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit feature';
            $data['edit_data'] = WebsiteFeature::where('slack', $slack)->first();
        }
        return view('website.feature.add', compact('data'));
    }
}
