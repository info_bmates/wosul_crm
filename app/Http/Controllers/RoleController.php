<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;
use App\Models\Role;
use App\Models\RoleMenu;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{

    public function index()
    {
        $data['title'] = 'Roles';

        return view('role.list', compact('data'));
    }

    public function add($slack = null)
    {

        check_access(['MM_USER_AND_ROLE', 'SM_ROLES', 'A_ADD_ROLE']);

        $data['title'] = 'Add Role';
        $data['edit_data'] = null;

        if ($slack != null) {
            $data['title'] = 'Edit Role';

            $role = Role::where('slack', $slack)->first();
            $edit_data = new RoleResource($role);
            $menus = RoleMenu::where('role_id', '=', $role->id)->pluck('menu_id')->toArray();
            $data['edit_data'] = collect($edit_data)->union(collect(['menus' => $menus]));
        }

        $menu_data = getMenus();
        $data['menus'] = $menu_data;

        return view('role.add', compact('data'));
    }
}
