<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsitePartner;
use App\Http\Resources\WebsitePartnerResource;

class WebsitePartnerController extends Controller
{

    public function index()
    {
        $data['title'] = 'Partner';

        // get all partners
        $partners = WebsitePartner::all();
        $data['partners'] = WebsitePartnerResource::collection($partners);

        return view('website.partner.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Partner';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit partner';
            $data['edit_data'] = WebsitePartner::where('slack', $slack)->first();
        }
        return view('website.partner.add', compact('data'));
    }
}
