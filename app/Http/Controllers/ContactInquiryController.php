<?php

namespace App\Http\Controllers;

use App\Models\ContactInquiry;
use Illuminate\Http\Request;

class ContactInquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ContactInquiry  $contactInquiry
     * @return \Illuminate\Http\Response
     */
    public function show(ContactInquiry $contactInquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContactInquiry  $contactInquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactInquiry $contactInquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ContactInquiry  $contactInquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactInquiry $contactInquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ContactInquiry  $contactInquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactInquiry $contactInquiry)
    {
        //
    }
}
