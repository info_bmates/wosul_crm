<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;
use DB;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    public function index()
    {
        $data['title'] = 'Login';
        return view('login', compact('data'));
    }

    public function authenticate(Request $request)
    {

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        try {

            $data = [];

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

                $user = Auth::user();

                $token =  $user->createToken('MyApp')->accessToken;

                $data['access_token'] = $token;

                $data['redirect_url'] = route('dashboard');

                /* loading an user session data */
                $menus = $this->get_user_menu($request, $user->id);
                session([
                    'logged_user_id' => $user->id,
                    'logged_user_name' => $user->name,
                    'logged_user_slack' => $user->slack,
                    'logged_user_role_id' => $user->role_id,
                    'logged_user_is_admin' => ($user->role_id == 1) ? true : false,
                    'logged_user_menus' => $menus
                ]);
            } else {

                throw new Exception("Invalid Username or Password");
            }


            return response()->json($this->generate_response(
                array(
                    "message" => "You are successfully logged in",
                    "data"    => $data
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {
            return response()->json($this->generate_response(
                [
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode(),
                ]
            ));
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }

    public function getUser()
    {
        return Auth::user();
    }

    public function get_user_menu($request, $user_id)
    {
        $menus = [];
        if ($request->logged_user_role_id == 1) {
            $menus = Menu::select('id')
                ->active()
                ->orderByRaw('FIELD(type , "MAIN_MENU", "SUB_MENU") ASC')
                ->orderBy('sort_order', 'ASC')
                ->get()->pluck('id')->toArray();
        } else {
            $menus = DB::table('user_menus')
                ->select('menus.id')
                ->leftJoin('menus', function ($join) {
                    $join->on('menus.id', '=', 'user_menus.menu_id');
                    $join->where('menus.status', '=', 1);
                })
                ->where('user_menus.user_id', $user_id)
                ->orderByRaw('FIELD(type , "MAIN_MENU", "SUB_MENU") ASC')
                ->orderBy('sort_order', 'ASC')
                ->get()->pluck('id')->toArray();
        }
        return $menus;
    }
}
