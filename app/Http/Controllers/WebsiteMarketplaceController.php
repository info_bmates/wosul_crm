<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteMarketplace;
use App\Http\Resources\WebsiteMarketplaceResource;

class WebsiteMarketplaceController extends Controller
{

    public function index()
    {
        $data['title'] = 'Devices';

        // get all marketplaces
        $marketplaces = WebsiteMarketplace::all();
        $data['marketplaces'] = WebsiteMarketplaceResource::collection($marketplaces);

        return view('website.marketplace.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Marketplace';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit Marketplace';
            $data['edit_data'] = WebsiteMarketplace::with('specifications')->where('slack', $slack)->first();
        }
        return view('website.marketplace.add', compact('data'));
    }
}
