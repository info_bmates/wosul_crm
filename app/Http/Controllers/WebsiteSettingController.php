<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteSetting;
use App\Http\Resources\WebsiteSettingResource;

class WebsiteSettingController extends Controller
{

    public function index()
    {
        $data['title'] = 'Setting';

        // get all settings
        $settings = WebsiteSetting::all();
        $data['settings'] = WebsiteSettingResource::collection($settings);

        return view('website.setting', compact('data'));
    }
}
