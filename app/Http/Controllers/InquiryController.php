<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;
use App\Models\WhatsappVisit;
use Illuminate\Support\Facades\DB;

class InquiryController extends Controller
{

    public function whatsapp_visits()
    {
        $data['title'] = 'Whatsapp Visits';

        return view('inquiry.whatsapp_visit', compact('data'));
    }
    
    public function demo_request()
    {
        $data['title'] = 'Demo Requests';

        return view('inquiry.demo_request', compact('data'));
    }

    
}
