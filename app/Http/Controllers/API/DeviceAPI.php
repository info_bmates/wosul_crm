<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\Device;
use App\Http\Resources\DeviceResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class DeviceAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $devices = Device::search($search_term)->paginate($paginate);
        return DeviceResource::collection($devices);
    }

    public function store(Request $request)
    {

        $rules = [
            'title' => ['required', 'string', 'unique:devices,title'],
            'title_ar' => ['required', 'string', 'unique:devices,title_ar'],
            'short_description' => ['required'],
            'short_description_ar' => ['required'],
            'amount' => ['required']
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $device = [
                'slack' => $this->generate_slack('devices'),
                'title' => $request->title,
                'title_ar' => $request->title_ar,
                'short_description' => $request->short_description,
                'short_description_ar' => $request->short_description_ar,
                'currency' => $request->currency,
                'amount' => $request->amount,
                'discount' => $request->discount,
                'discount_description' => $request->discount_description,
                'discount_description_ar' => $request->discount_description_ar,
                'is_featured' => $request->is_featured,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $device = Device::create($device);

            if (isset($request->image)) {

                $device_image = $request->image;
                $extension = $device_image->getClientOriginalExtension();
                $file_name = $device['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('device')->putFileAs('/', $device_image, $file_name);
                $file_name = basename($path);

                Device::where('id', $device->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Device has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'title' => ['required', 'string',  Rule::unique('devices')->ignore($request->slack, 'slack')],
            'title_ar' => ['required', 'string',  Rule::unique('devices')->ignore($request->slack, 'slack')],
            'short_description' => ['required'],
            'short_description_ar' => ['required'],
            'amount' => ['required']
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $device = Device::where('slack', $request->slack)->first();

            $device->title = $request->title;
            $device->title_ar = $request->title_ar;
            $device->short_description = $request->short_description;
            $device->short_description_ar = $request->short_description_ar;
            $device->currency = $request->currency;
            $device->amount = $request->amount;
            $device->discount = ($request->discount != 'null') ? $request->discount : 0;
            $device->discount_description = ($request->discount_description != 'null') ? $request->discount_description : '';
            $device->discount_description_ar = ($request->discount_description_ar != 'null') ? $request->discount_description_ar : '';
            $device->is_featured = $request->is_featured;
            $device->status = $request->status;
            $device->updated_by = Auth::guard('api_user')->user()->id;
            $device->save();

            if (isset($request->image)) {

                $device_image = $request->image;
                Storage::disk('device')->delete($request->image);
                $extension = $device_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('device')->putFileAs('/', $device_image, $file_name);
                $file_name = basename($path);

                Device::where('id', $device->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Device been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
