<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteFeature;
use App\Http\Resources\WebsiteFeatureResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteFeatureAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $features = WebsiteFeature::search($search_term)->paginate($paginate);

        return WebsiteFeatureResource::collection($features);
    }

    public function store(Request $request)
    {

        $rules = [
            'title' => ['required', 'string', 'unique:website_features,title'],
            'title_ar' => ['required', 'string', 'unique:website_features,title_ar'],

        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $feature = [
                'slack' => $this->generate_slack('website_features'),
                'title' => $request->title,
                'title_ar' => $request->title_ar,
                'description' => $request->description,
                'description_ar' => $request->description_ar,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $feature = WebsiteFeature::create($feature);

            if (isset($request->image)) {

                $feature_image = $request->image;
                $extension = $feature_image->getClientOriginalExtension();
                $file_name = $feature['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_feature')->putFileAs('/', $feature_image, $file_name);
                $file_name = basename($path);

                WebsiteFeature::where('id', $feature->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Feature has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {


        $rules = [
            'title' => ['required', 'string',  Rule::unique('website_features')->ignore($request->slack, 'slack')],
            'title_ar' => ['required', 'string',  Rule::unique('website_features')->ignore($request->slack, 'slack')],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $feature = WebsiteFeature::where('slack', $request->slack)->first();

            $feature->title = $request->title;
            $feature->title_ar = $request->title_ar;
            $feature->description = $request->description;
            $feature->description_ar = $request->description_ar;
            $feature->status = $request->status;
            $feature->updated_by = Auth::guard('api_user')->user()->id;
            $feature->save();

            if (isset($request->image) && $request->image != null) {

                $feature_image = $request->image;

                Storage::disk('website_feature')->delete($request->image);
                $extension = $feature_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_feature')->putFileAs('/', $feature_image, $file_name);
                $file_name = basename($path);

                WebsiteFeature::where('id', $feature->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Feature been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
