<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Exception;
use DB;
use App\Http\Controllers\API\RoleAPI;

class UserAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'role' => '',
            'status' => ''
        ]);
        $users = User::exceptAdminUser()->withoutTrashed()->search($search_term)->paginate($paginate);
        return UserResource::collection($users);
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', Rule::unique('users')],
            'phone' => ['nullable', Rule::unique('users')],
            'password' => ['required', 'min:6'],
            'status' => ['required'],
            'role_id' => ['required'],
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $user = [
                'slack' => $this->generate_slack('users'),
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone' => $request->phone,
                'role_id' => $request->role_id,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $user = User::create($user);

            $role_api = new RoleAPI();
            $role_api->update_user_roles($user->role_id, $user->id);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "User has been created successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', Rule::unique('users')->ignore($request->slack, 'slack')],
            'phone' => ['nullable', Rule::unique('users')->ignore($request->slack, 'slack')],
            'status' => ['required'],
            'role_id' => ['required'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $user = User::where('slack', $request->slack)->first();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->role_id = $request->role_id;
            $user->status = $request->status;
            $user->updated_by = Auth::guard('api_user')->user()->id;
            if ($request->password != '') {
                $user->password = Hash::make($request->password);
            }
            $user->save();

            $role_api = new RoleAPI();
            $role_api->update_user_roles($user->role_id, $user->id);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "User has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function switchThemeMode(Request $request)
    {
        User::where('slack', request()->user()->slack)->update(['dark_mode' => $request->dark_mode]);
    }
}
