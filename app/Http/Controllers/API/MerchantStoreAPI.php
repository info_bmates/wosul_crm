<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Illuminate\Http\Request;
use Exception;

class MerchantStoreAPI extends Controller
{
    public function update_status(Request $request)
    {
        try {

            check_access(['MM_MERCHANT']);

            /* Getting the data from merchant database : start */
            $db = createConnection($request->merchant_db);

            $db->where('slack', $request->slack);
            $db->update('stores', [
                'status' => $request->status
            ]);

            closeConnection($db);
            /* Getting the data from merchant database : ends */

            return response()->json($this->generate_response(
                array(
                    "message" => ($request->status == 1) ? "Store Activated Successfully!" : "Store Deactivated Successfully!",
                    "data" => ['status' => $request->status]
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
