<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\Subscription;
use App\Models\SubscriptionMenu;
use App\Http\Resources\SubscriptionResource;
use App\Models\SubscriptionFeature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class SubscriptionAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $subscriptions = Subscription::search($search_term)->paginate($paginate);
        return SubscriptionResource::collection($subscriptions);
    }

    public function store(Request $request)
    {

        $rules = [
            'title' => ['required', 'string', 'unique:subscriptions,title'],
            'title_ar' => ['required', 'string', 'unique:subscriptions,title_ar'],
            'short_description' => ['required'],
            'short_description_ar' => ['required'],
            'subscription_type' => ['required'],
            'subscription_tenure' => ['required'],
            'currency' => ['required'],
            'amount' => ['required'],
            'features' => ['required'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $subscription = [
                'slack' => $this->generate_slack('subscriptions'),
                'title' => $request->title,
                'title_ar' => $request->title_ar,
                'short_description' => $request->short_description,
                'short_description_ar' => $request->short_description_ar,
                'subscription_type' => $request->subscription_type,
                'subscription_tenure' => $request->subscription_tenure,
                'currency' => $request->currency,
                'amount' => $request->amount,
                'discount' => $request->discount,
                'discount_description' => $request->discount_description,
                'discount_description_ar' => $request->discount_description_ar,
                'is_featured' => $request->is_featured,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $subscription = Subscription::create($subscription);

            $features = json_decode($request->features, true);
            if (isset($features)) {

                $features_array = [];
                foreach ($features as $feature) {
                    $dataset['subscription_id'] = $subscription->id;
                    $dataset['title'] = $feature['title'];
                    $dataset['title_ar'] = $feature['title_ar'];
                    $features_array[] = $dataset;
                }
            }

            SubscriptionFeature::insert($features_array);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Subscription has been created successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'title' => ['required', 'string',  Rule::unique('subscriptions')->ignore($request->slack, 'slack')],
            'title_ar' => ['required', 'string',  Rule::unique('subscriptions')->ignore($request->slack, 'slack')],
            'short_description' => ['required'],
            'short_description_ar' => ['required'],
            'subscription_type' => ['required'],
            'subscription_tenure' => ['required'],
            'currency' => ['required'],
            'amount' => ['required']
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $subscription = Subscription::where('slack', $request->slack)->first();

            $subscription->title = $request->title;
            $subscription->title_ar = $request->title_ar;
            $subscription->short_description = $request->short_description;
            $subscription->short_description_ar = $request->short_description_ar;
            $subscription->subscription_type = $request->subscription_type;
            $subscription->subscription_tenure = $request->subscription_tenure;
            $subscription->currency = $request->currency;
            $subscription->amount = $request->amount;
            $subscription->discount = ($request->discount != 'null') ? $request->discount : 0;
            $subscription->discount_description = ($request->discount_description != 'null') ? $request->discount_description : '';
            $subscription->discount_description_ar = ($request->discount_description_ar != 'null') ? $request->discount_description_ar : '';
            $subscription->is_featured = $request->is_featured;
            $subscription->status = $request->status;
            $subscription->updated_by = Auth::guard('api_user')->user()->id;
            $subscription->save();

            $features = json_decode($request->features, true);
            if (isset($features)) {

                $features_array = [];
                foreach ($features as $feature) {
                    $dataset['subscription_id'] = $subscription->id;
                    $dataset['title'] = $feature['title'];
                    $dataset['title_ar'] = $feature['title_ar'];
                    $features_array[] = $dataset;
                }
            }

            SubscriptionFeature::where('subscription_id', $subscription->id)->forceDelete();
            SubscriptionFeature::insert($features_array);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Subscription has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update_menus(Request $request)
    {

        try {

            DB::beginTransaction();

            $subscription = Subscription::select('*')
                ->where([
                    ['slack', '=', $request->slack]
                ])
                ->first();

            $subscription_menus = explode(",", $request->subscription_menus);

            if (count($subscription_menus) > 0) {
                $subscription_menus = array_unique($subscription_menus);
                $subscription_menu_array = [];
                foreach ($subscription_menus as $subscription_menu) {
                    $menu = [
                        'subscription_id' => $subscription->id,
                        'menu_id' => $subscription_menu,
                        'created_by' => $request->logged_user_id
                    ];
                    $subscription_menu_array[] = $menu;
                }

                SubscriptionMenu::where('subscription_id', $subscription->id)->delete();
                SubscriptionMenu::insert($subscription_menu_array);
            }

            DB::commit();
            return response()->json($this->generate_response(
                array(
                    "message" => "Subscription Menus has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
