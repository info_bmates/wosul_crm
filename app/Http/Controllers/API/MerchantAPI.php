<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Illuminate\Http\Request;
use Exception;

class MerchantAPI extends Controller
{
    public function index()
    {
        $data['action_key'] = 'MM_MERCHANT';
        if (check_access(array($data['action_key']), true) == false) {
            $response = $this->no_access_response_for_listing_table();
            return $response;
        }

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $users = Merchant::withoutTrashed()->search($search_term)->paginate($paginate);
        return MerchantResource::collection($users);
    }

    public function update_status(Request $request)
    {
        try {

            check_access(['MM_MERCHANT', 'A_VIEW_MERCHANT']);
            Merchant::where('slack', $request->slack)->update(['status' => $request->status]);

            return response()->json($this->generate_response(
                array(
                    "message" => ($request->status == 1) ? "Merchant Activated Successfully!" : "Merchant Deactivated Successfully!",
                    "data" => ['status' => $request->status]
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
