<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteReview;
use App\Http\Resources\WebsiteReviewResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteReviewAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $reviews = WebsiteReview::search($search_term)->paginate($paginate);

        return WebsiteReviewResource::collection($reviews);
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => ['required', 'string', 'unique:website_reviews,name'],
            'name_ar' => ['required', 'string', 'unique:website_reviews,name_ar'],
            'review' => ['required'],
            'review_ar' => ['required'],
            'rating' => ['required']
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $review = [
                'slack' => $this->generate_slack('website_reviews'),
                'name' => $request->name,
                'name_ar' => $request->name_ar,
                'review' => $request->review,
                'review_ar' => $request->review_ar,
                'rating' => $request->rating,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $review = WebsiteReview::create($review);

            if (isset($request->image)) {

                $review_image = $request->image;
                $extension = $review_image->getClientOriginalExtension();
                $file_name = $review['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_review')->putFileAs('/', $review_image, $file_name);
                $file_name = basename($path);

                WebsiteReview::where('id', $review->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Review has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {


        $rules = [
            'name' => ['required', 'string'],
            'name_ar' => ['required', 'string'],
            'review' => ['required'],
            'review_ar' => ['required'],
            'rating' => ['required'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $review = WebsiteReview::where('slack', $request->slack)->first();

            $review->name = $request->name;
            $review->name_ar = $request->name_ar;
            $review->review = $request->review;
            $review->review_ar = $request->review_ar;
            $review->rating = $request->rating;
            $review->status = $request->status;
            $review->updated_by = Auth::guard('api_user')->user()->id;
            $review->save();

            if (isset($request->image) && $request->image != null) {

                $review_image = $request->image;

                Storage::disk('website_review')->delete($request->image);
                $extension = $review_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_review')->putFileAs('/', $review_image, $file_name);
                $file_name = basename($path);

                WebsiteReview::where('id', $review->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Review been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
