<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteService;
use App\Http\Resources\WebsiteServiceResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteServiceAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $services = WebsiteService::search($search_term)->paginate($paginate);

        return WebsiteServiceResource::collection($services);
    }

    public function store(Request $request)
    {

        $rules = [
            'title' => ['required', 'string'],
            'title_ar' => ['required', 'string'],
            'description' => ['required'],
            'description_ar' => ['required'],
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $service = [
                'slack' => $this->generate_slack('website_services'),
                'title' => $request->title,
                'title_ar' => $request->title_ar,
                'description' => $request->description,
                'description_ar' => $request->description_ar,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $service = WebsiteService::create($service);

            if (isset($request->image)) {

                $service_image = $request->image;
                $extension = $service_image->getClientOriginalExtension();
                $file_name = $service['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_service')->putFileAs('/', $service_image, $file_name);
                $file_name = basename($path);

                WebsiteService::where('id', $service->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Service has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'title' => ['required', 'string',  Rule::unique('website_services')->ignore($request->slack, 'slack')],
            'title_ar' => ['required', 'string',  Rule::unique('website_services')->ignore($request->slack, 'slack')],
            'description' => ['required'],
            'description_ar' => ['required'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $service = WebsiteService::where('slack', $request->slack)->first();

            $service->title = $request->title;
            $service->title_ar = $request->title_ar;
            $service->description = $request->description;
            $service->description_ar = $request->description_ar;
            $service->status = $request->status;
            $service->updated_by = Auth::guard('api_user')->user()->id;
            $service->save();

            if (isset($request->image) && $request->image != null) {

                $service_image = $request->image;

                Storage::disk('website_service')->delete($request->image);
                $extension = $service_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_service')->putFileAs('/', $service_image, $file_name);
                $file_name = basename($path);

                WebsiteService::where('id', $service->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Service been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
