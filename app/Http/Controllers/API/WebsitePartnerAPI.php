<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsitePartner;
use App\Http\Resources\WebsitePartnerResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsitePartnerAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $partners = WebsitePartner::search($search_term)->paginate($paginate);

        return WebsitePartnerResource::collection($partners);
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => ['required', 'string', 'unique:website_partners,name']
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $partner = [
                'slack' => $this->generate_slack('website_partners'),
                'name' => $request->name,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $partner = WebsitePartner::create($partner);

            if (isset($request->image)) {

                $partner_image = $request->image;
                $extension = $partner_image->getClientOriginalExtension();
                $file_name = $partner['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_partner')->putFileAs('/', $partner_image, $file_name);
                $file_name = basename($path);

                WebsitePartner::where('id', $partner->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Partner has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {


        $rules = [
            'name' => ['required', 'string',  Rule::unique('website_partners')->ignore($request->slack, 'slack')],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $partner = WebsitePartner::where('slack', $request->slack)->first();

            $partner->name = $request->name;
            $partner->status = $request->status;
            $partner->updated_by = Auth::guard('api_user')->user()->id;
            $partner->save();

            if (isset($request->image) && $request->image != null) {

                $partner_image = $request->image;

                Storage::disk('website_partner')->delete($request->image);
                $extension = $partner_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_partner')->putFileAs('/', $partner_image, $file_name);
                $file_name = basename($path);

                WebsitePartner::where('id', $partner->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Partner has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
