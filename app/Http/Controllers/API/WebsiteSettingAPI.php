<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteSetting;
use App\Http\Resources\WebsiteSettingResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteSettingAPI extends Controller
{

    public function update(Request $request)
    {

        try {

            DB::beginTransaction();

            $settings  = json_decode($request->settings);

            foreach ($settings as $setting) {
                if ($setting->type == 'TEXT') {
                    $value = $setting->value;
                    WebsiteSetting::where('key', $setting->key)->update(['value' => $value]);
                }
            }


            if (isset($request->header_logo) && $request->header_logo != null) {
                $header_logo = $request->header_logo;
                Storage::disk('website_setting')->delete($header_logo);
                $extension = $header_logo->getClientOriginalExtension();
                $file_name = uniqid() . '.' . $extension;
                $path = Storage::disk('website_setting')->putFileAs('/', $header_logo, $file_name);
                $value = basename($path);
                WebsiteSetting::where('key', 'HEADER_LOGO')->update(['value' => $value]);
            }

            if (isset($request->footer_logo) && $request->footer_logo != null) {
                $footer_logo = $request->footer_logo;
                Storage::disk('website_setting')->delete($footer_logo);
                $extension = $footer_logo->getClientOriginalExtension();
                $file_name = uniqid() . '.' . $extension;
                $path = Storage::disk('website_setting')->putFileAs('/', $footer_logo, $file_name);
                $value = basename($path);
                WebsiteSetting::where('key', 'FOOTER_LOGO')->update(['value' => $value]);
            }

            if (isset($request->favicon_icon) && $request->favicon_icon != null) {
                $favicon_icon = $request->favicon_icon;
                Storage::disk('website_setting')->delete($favicon_icon);
                $extension = $favicon_icon->getClientOriginalExtension();
                $file_name = uniqid() . '.' . $extension;
                $path = Storage::disk('website_setting')->putFileAs('/', $favicon_icon, $file_name);
                $value = basename($path);
                WebsiteSetting::where('key', 'FAVICON_ICON')->update(['value' => $value]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Setting has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
