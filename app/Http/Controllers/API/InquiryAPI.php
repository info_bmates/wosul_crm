<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DemoRequestResource;
use App\Http\Resources\WhatsappVisitResource;
use App\Models\DemoRequest;
use App\Models\WhatsappVisit;
use App\Models\RoleMenu;
use App\Models\UserMenu;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class InquiryAPI extends Controller
{

    public function whatsapp_visits()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        
        $whatsapp_visits = WhatsappVisit::search($search_term)->paginate($paginate);

        return WhatsappVisitResource::collection($whatsapp_visits);
    }
    
    public function demo_requests()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        
        $demo_requests = DemoRequest::search($search_term)->orderBy('created_at','DESC')->paginate($paginate);

        return DemoRequestResource::collection($demo_requests);
    }

}
