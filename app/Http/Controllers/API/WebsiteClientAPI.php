<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteClient;
use App\Http\Resources\WebsiteClientResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteClientAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $clients = WebsiteClient::search($search_term)->paginate($paginate);

        return WebsiteClientResource::collection($clients);
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => ['required', 'string', 'unique:website_clients,name']
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $client = [
                'slack' => $this->generate_slack('website_clients'),
                'name' => $request->name,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $client = WebsiteClient::create($client);

            if (isset($request->image)) {

                $client_image = $request->image;
                $extension = $client_image->getClientOriginalExtension();
                $file_name = $client['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_client')->putFileAs('/', $client_image, $file_name);
                $file_name = basename($path);

                WebsiteClient::where('id', $client->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Client has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {


        $rules = [
            'name' => ['required', 'string',  Rule::unique('website_clients')->ignore($request->slack, 'slack')],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $client = WebsiteClient::where('slack', $request->slack)->first();

            $client->name = $request->name;
            $client->status = $request->status;
            $client->updated_by = Auth::guard('api_user')->user()->id;
            $client->save();

            if (isset($request->image) && $request->image != null) {

                $client_image = $request->image;

                Storage::disk('website_client')->delete($request->image);
                $extension = $client_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_client')->putFileAs('/', $client_image, $file_name);
                $file_name = basename($path);

                WebsiteClient::where('id', $client->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Client has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
