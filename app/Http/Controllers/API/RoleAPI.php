<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use App\Models\RoleMenu;
use App\Models\UserMenu;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RoleAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $roles = Role::search($search_term)->paginate($paginate);
        return RoleResource::collection($roles);
    }

    public function store(Request $request)
    {

        $rules = [
            'role_name' => ['required', 'string', 'min:5', 'max:20', 'unique:roles,label'],
            'role_code' => ['required', 'min:3', Rule::unique('roles')],
            'role_menus' => ['required'],
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $role = [
                'slack' => $this->generate_slack('roles'),
                'role_code' => $request->role_code,
                'label' => $request->role_name
            ];

            $role = Role::create($role);

            $role_menus = explode(",", $request->role_menus);

            if (count($role_menus) > 0) {
                $role_menus = array_unique($role_menus);
                foreach ($role_menus as $role_menu) {
                    $menu = [
                        'role_id' => $role->id,
                        'menu_id' => $role_menu,
                        'created_by' => $request->logged_user_id
                    ];
                    RoleMenu::create($menu);
                }
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Role has been created successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'role_name' => ['required', 'string', 'min:5', 'max:20'],
            'role_code' => ['required', 'min:3'],
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $role = [
                'role_code' => $request->role_code,
                'label' => $request->role_name
            ];

            Role::where('slack', $request->slack)->update($role);

            $role_details = Role::select('*')
                ->where([
                    ['slack', '=', $request->slack]
                ])
                ->first();
            $role_current_menus = RoleMenu::where('role_id', '=', $role_details->id)->pluck('menu_id')->toArray();
            (count($role_current_menus) > 0) ? sort($role_current_menus) : $role_current_menus;

            $role_menus = explode(",", $request->role_menus);
            (count($role_menus) > 0) ? sort($role_menus) : $role_menus;

            if (count($role_menus) > 0 && ($role_current_menus != $role_menus)) {

                RoleMenu::where('role_id', $role_details->id)->delete();

                $role_menus = array_unique($role_menus);

                foreach ($role_menus as $role_menu) {
                    $menu = [
                        'role_id' => $role_details->id,
                        'menu_id' => $role_menu,
                        'created_by' => $request->logged_user_id
                    ];
                    RoleMenu::create($menu);
                }
            }

            $this->update_user_roles($role_details->id);

            DB::commit();
            return response()->json($this->generate_response(
                array(
                    "message" => "Role has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update_user_roles($role_id, $user_id = null)
    {

        if ($user_id == '') {
            $users = User::where('role_id', $role_id)->get();
        } else {
            $users = User::where('id', $user_id)->get();
        }

        $role_menus = RoleMenu::where('role_id', $role_id)->get();

        foreach ($users as $user) {

            $user_menus_array = [];

            if (isset($role_menus)) {
                foreach ($role_menus as $role_menu) {
                    $dataset['user_id'] = $user->id;
                    $dataset['menu_id'] = $role_menu->menu_id;
                    $user_menus_array[] = $dataset;
                }
            }

            UserMenu::where('user_id', $user->id)->delete();
            UserMenu::insert($user_menus_array);
        }
    }
}
