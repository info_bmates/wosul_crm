<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use App\Http\Resources\MerchantResource;
use App\Models\Merchant;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Exports\RevenueExport;
use App\Exports\RegistrationExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;

class ReportAPI extends Controller
{

    public function __construct()
    {
        $this->view_path = Config::get('constants.upload.reports.view_path');
    }

    public function engagement_report()
    {

        try {
            
            $paginate = request('paginate', config('constants.pagination.items_per_page'));
            $search_term = request('query', [
                'search_term' => '',
                'status' => ''
            ]);
    
            $dates = json_decode(request('dates'), true);
            $start_date = Carbon::parse($dates['startDate'])->format('Y-m-d');
            $end_date = Carbon::parse($dates['endDate'])->format('Y-m-d');

            $status = (boolean) json_decode(request('status'));

            check_access(['MM_REPORT','SM_ENGAGEMENT_REPORT']);
            
            $merchants = Merchant::select('company_url')->active()->get();

            $data = [];
            $merchant_count = 0;
            $inmerchant_count = 0;
            $no_databases_found = [];

            if(isset($merchants)){
           
                foreach($merchants as $merchant){
           
                    /* Getting the data from merchant database : start */
                    $merchant_dbname = Str::lower($merchant->company_url)."_wosul";
                    // check if database exists or not
                    $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
                    $db = DB::select($query, [$merchant_dbname]);
                    if (!empty($db)) {

                        $db = createConnection($merchant_dbname);
                        
                        $dataset['merchant'] = Str::ucfirst($merchant->company_url);
        
                        // fetch total orders
                        $db->where('DATE(value_date)', $start_date, '>=');
                        $db->where('DATE(value_date)', $end_date, '<=');
                        $dataset['total_orders'] = $db->getValue('orders','count(*)');
    
                        // fetch total invoices
                        $db->where('DATE(invoice_date)', $start_date, '>=');
                        $db->where('DATE(invoice_date)', $end_date, '<=');
                        $dataset['total_invoices'] = $db->getValue('invoices','count(*)');
                        
                        // fetch total purchase orders
                        $db->where('DATE(created_at)', $start_date, '>=');
                        $db->where('DATE(created_at)', $end_date, '<=');
                        $dataset['total_purchase_orders'] = $db->getValue('purchase_orders','count(*)');
                        
                        // // fetch total quantity purchases
                        // $db->where('DATE(created_at)', $start_date, '>=');
                        // $db->where('DATE(created_at)', $end_date, '<=');
                        // $dataset['total_quantity_purchases'] = $db->getValue('quantity_purchases','count(*)');
                        
                        // fetch total users
                        $db->where('DATE(created_at)', $start_date, '>=');
                        $db->where('DATE(created_at)', $end_date, '<=');
                        $dataset['total_users'] = $db->getValue('users','count(*)');

                        if($dataset['total_orders'] > 0 || $dataset['total_invoices'] > 0 || $dataset['total_purchase_orders'] > 0 || $dataset['total_users'] > 0 ){
                            $dataset['status'] = 'Active';
                            $merchant_count++;
                        }else{
                            $dataset['status'] = 'Inactive';
                            $inmerchant_count++;
                        }
                        
                        if(!$status){
                            $data[] = $dataset;
                        }else{
                            if($dataset['status'] == 'Inactive'){
                                $data[] = $dataset;
                            }
                        }
    
                        closeConnection($db);
                        /* Getting the data from merchant database : ends */

                    }else{  
                        $no_databases_found[] = $merchant_dbname;
                    }

                }
            }

            return response()->json($this->generate_response(
                array(
                    "message" => "Report Generated Successfully",
                    "data" => [
                        'data'=>$data,
                        'merchant_count'=>$merchant_count,
                        'inmerchant_count'=>$inmerchant_count,
                        'no_databases_found'=>$no_databases_found,
                        ]
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
        
    }

    public function revenue_report()
    {
        
        try {
    
            $merchant_id = request('merchant_id');

            $from_year = request('from_year');
            $from_month = request('from_month');
            
            $to_year = request('to_year');
            $to_month = request('to_month');

            $month_names = [
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'Septemeber',
                10 => 'October',
                11 => 'Novemeber',
                12 => 'December'
            ]; 

            $month_range = [];
            if($from_year == $to_year){
                $month_range = range($from_month,$to_month); 
            }else{
                echo "In Pending State";
                exit;
            }
            
            // check_access(['MM_REPORT','SM_REVENUE_REPORT']);

            if(isset($month_range)){
                
                $merchants = Merchant::select('company_url')
                ->when( isset($merchant_id) && $merchant_id != null , function($query) use($merchant_id) {
                    $query->where('id',$merchant_id);
                })
                ->active()->get();
                
                $data = [];
                $merchant_count = 0;
                $no_databases_found = [];
    
                if(isset($merchants)){
               
                    foreach($merchants as $merchant){
               
                        /* Getting the data from merchant database : start */
                        $merchant_dbname = Str::lower($merchant->company_url)."_wosul";
                        // check if database exists or not
                        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
                        $db = DB::select($query, [$merchant_dbname]);
                        if (!empty($db)) {
    
                            $db = createConnection($merchant_dbname);
                            
                            $dataset['merchant'] = Str::ucfirst($merchant->company_url);

                            foreach($month_range as $month){

                                $dataset['month_and_year'] = $month_names[$month].", ".$from_year;
                                
                                // total order revenue
                                $db->where('YEAR(value_date)', $from_year);
                                $db->where('MONTH(value_date)', $month);
                                $db->where('status',1);
                                $total_order_revenue = $db->getValue('orders','SUM(total_order_amount)') ?? 0;
                                $dataset['total_order_revenue'] = number_format($total_order_revenue,2);
                                
                                // order revenue in change
                                $db->where('YEAR(value_date)', $from_year);
                                $db->where('MONTH(value_date)', $month);
                                $db->where('status',1);
                                $order_revenue_in_change = $db->getValue('orders','SUM(change_amount)') ?? 0;
                                
                                // order revenue in cash
                                $db->where('YEAR(value_date)', $from_year);
                                $db->where('MONTH(value_date)', $month);
                                $db->where('status',1);
                                $order_revenue_in_cash = (string)  ($db->getValue('orders','SUM(cash_amount)') - $order_revenue_in_change) ?? 0;
                                $dataset['order_revenue_in_cash'] = number_format($order_revenue_in_cash,2);

                                // order revenue in credit
                                $db->where('YEAR(value_date)', $from_year);
                                $db->where('MONTH(value_date)', $month);
                                $db->where('status',1);
                                $order_revenue_in_credit = $db->getValue('orders','SUM(credit_amount)') ?? 0;
                                $dataset['order_revenue_in_credit'] = number_format($order_revenue_in_credit,2);
                                
                                // returned order amount
                                $db->where('YEAR(value_date)', $from_year);
                                $db->where('MONTH(value_date)', $month);
                                $order_return_amount = $db->getValue('orders','SUM(return_order_amount)') ?? 0;
                                $dataset['order_return_amount'] = number_format($order_return_amount,2);

                                // fetch total users
                                $dataset['total_branches'] = $db->getValue('stores','count(*)') ?? 0;
                                $data[] = $dataset;
                            
                            }

                            
                            $merchant_count++;
                            
                            closeConnection($db);
                            /* Getting the data from merchant database : ends */
    
                        }else{  
                            $no_databases_found[] = $merchant_dbname;
                        }
    
                    }
                }
            
            }
            
            $filename = 'revenue_report_' . date('Y_m_d_h_i_s') . '_' . uniqid() . '.xlsx';

            Excel::store(
                new RevenueExport(
                    [$data]
                ),
                'public/reports/' . $filename
            );

            $download_link = asset($this->view_path . $filename);

            return response()->json($this->generate_response(
                array(
                    "message" => "Report Generated Successfully",
                    "data" => [
                        'data'=>$data,
                        'merchant_count'=>$merchant_count,
                        'no_databases_found'=>$no_databases_found,
                        'download_link' => $download_link
                        ]
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
        
    }

    public function registration_report()
    {

        try {
            
            $paginate = request('paginate', config('constants.pagination.items_per_page'));
            $search_term = request('query', [
                'search_term' => '',
                'status' => ''
            ]);
    
            $dates = json_decode(request('dates'), true);
            $start_date = Carbon::parse($dates['startDate'])->format('Y-m-d');
            $end_date = Carbon::parse($dates['endDate'])->format('Y-m-d');

            check_access(['MM_REPORT','SM_REGISTRATION_REPORT']);
            
            $merchants = Merchant::query()
            ->whereBetween( DB::Raw('DATE(created_at)'),[$start_date,$end_date])
            ->orderBy('created_at','DESC');
            $data['merchants'] = MerchantResource::collection($merchants->get());

            $filename = 'registration_report_' . date('Y_m_d_h_i_s') . '_' . uniqid() . '.xlsx';

            Excel::store(
                new RegistrationExport(
                    [$merchants]
                ),
                'public/reports/' . $filename
            );

            $download_link = asset($this->view_path . $filename);


            return response()->json($this->generate_response(
                array(
                    "message" => "Report Generated Successfully",
                    "data" => [
                        'data'=>$data,
                        'download_link'=>$download_link,
                        ]
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    
    }

}
