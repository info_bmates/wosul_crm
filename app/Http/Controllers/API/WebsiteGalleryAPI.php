<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteGallery;
use App\Http\Resources\WebsiteGalleryResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteGalleryAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $gallery = WebsiteGallery::search($search_term)->paginate($paginate);

        return WebsiteGalleryResource::collection($gallery);
    }

    public function store(Request $request)
    {

        $rules = [
            'size' => ['required', 'string']
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $gallery = [
                'slack' => $this->generate_slack('website_gallery'),
                'size' => $request->size,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $gallery = WebsiteGallery::create($gallery);

            if (isset($request->image)) {

                $gallery_image = $request->image;
                $extension = $gallery_image->getClientOriginalExtension();
                $file_name = $gallery['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_gallery')->putFileAs('/', $gallery_image, $file_name);
                $file_name = basename($path);

                WebsiteGallery::where('id', $gallery->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Gallery has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {


        $rules = [
            'size' => ['required', 'string'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $gallery = WebsiteGallery::where('slack', $request->slack)->first();

            $gallery->size = $request->size;
            $gallery->status = $request->status;
            $gallery->updated_by = Auth::guard('api_user')->user()->id;
            $gallery->save();

            if (isset($request->image) && $request->image != null) {

                $gallery_image = $request->image;

                Storage::disk('website_gallery')->delete($request->image);
                $extension = $gallery_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_gallery')->putFileAs('/', $gallery_image, $file_name);
                $file_name = basename($path);

                WebsiteGallery::where('id', $gallery->id)->update(['image' => $file_name]);
            }

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Gallery has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
