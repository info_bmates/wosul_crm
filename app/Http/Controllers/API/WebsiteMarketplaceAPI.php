<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\Models\WebsiteMarketplace;
use App\Http\Resources\WebsiteMarketplaceResource;
use App\Models\WebsiteMarketplaceSpecification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class WebsiteMarketplaceAPI extends Controller
{

    public function index()
    {

        $paginate = request('paginate', config('constants.pagination.items_per_page'));
        $search_term = request('query', [
            'search_term' => '',
            'status' => ''
        ]);
        $marketplaces = WebsiteMarketplace::search($search_term)->paginate($paginate);
        return WebsiteMarketplaceResource::collection($marketplaces);
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => ['required', 'string'],
            'title_ar' => ['required', 'string'],
            'short_description' => ['required', 'string'],
            'short_description_ar' => ['required', 'string'],
            'long_description' => ['required'],
            'long_description_ar' => ['required'],
        ];

        $data = [];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $marketplace = [
                'slack' => $this->generate_slack('website_marketplaces'),
                'title' => $request->title,
                'title_ar' => $request->title_ar,
                'short_description' => $request->short_description,
                'short_description_ar' => $request->short_description_ar,
                'long_description' => $request->long_description,
                'long_description_ar' => $request->long_description_ar,
                'status' => $request->status,
                'created_by' => Auth::guard('api_user')->user()->id,
            ];

            $marketplace = WebsiteMarketplace::create($marketplace);

            if (isset($request->thumb_image)) {

                $marketplace_image = $request->thumb_image;
                $extension = $marketplace_image->getClientOriginalExtension();
                $file_name = $marketplace['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_marketplace')->putFileAs('/', $marketplace_image, $file_name);
                $file_name = basename($path);

                WebsiteMarketplace::where('id', $marketplace->id)->update(['thumb_image' => $file_name]);
            }

            if (isset($request->banner_image)) {

                $marketplace_image = $request->banner_image;
                $extension = $marketplace_image->getClientOriginalExtension();
                $file_name = $marketplace['slack'] . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_marketplace')->putFileAs('/', $marketplace_image, $file_name);
                $file_name = basename($path);

                WebsiteMarketplace::where('id', $marketplace->id)->update(['banner_image' => $file_name]);
            }

            $specifications = json_decode($request->specifications, true);

            if (isset($specifications)) {

                $specifications_array = [];
                foreach ($specifications as $specification) {
                    $dataset['marketplace_id'] = $marketplace->id;
                    $dataset['name'] = $specification['name'];
                    $dataset['name_ar'] = $specification['name_ar'];
                    $dataset['value'] = $specification['value'];
                    $dataset['value_ar'] = $specification['value_ar'];
                    $specifications_array[] = $dataset;
                }
            }

            WebsiteMarketplaceSpecification::insert($specifications_array);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Marketplace has been added successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }

    public function update(Request $request)
    {

        $rules = [
            'title' => ['required', 'string',  Rule::unique('website_marketplaces')->ignore($request->slack, 'slack')],
            'title_ar' => ['required', 'string',  Rule::unique('website_marketplaces')->ignore($request->slack, 'slack')],
            'short_description' => ['required'],
            'short_description_ar' => ['required'],
            'long_description' => ['required'],
            'long_description_ar' => ['required'],
        ];

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), $rules);
            $validation_status = $validator->fails();
            if ($validation_status) {
                throw new Exception($validator->errors());
            }

            $marketplace = WebsiteMarketplace::where('slack', $request->slack)->first();

            $marketplace->title = $request->title;
            $marketplace->title_ar = $request->title_ar;
            $marketplace->short_description = $request->short_description;
            $marketplace->short_description_ar = $request->short_description_ar;
            $marketplace->long_description = $request->long_description;
            $marketplace->long_description_ar = $request->long_description_ar;
            $marketplace->status = $request->status;
            $marketplace->updated_by = Auth::guard('api_user')->user()->id;
            $marketplace->save();

            if (isset($request->thumb_image) && $request->thumb_image != null) {

                $marketplace_image = $request->thumb_image;

                Storage::disk('website_marketplace')->delete($request->thumb_image);
                $extension = $marketplace_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_marketplace')->putFileAs('/', $marketplace_image, $file_name);
                $file_name = basename($path);

                WebsiteMarketplace::where('id', $marketplace->id)->update(['thumb_image' => $file_name]);
            }

            if (isset($request->banner_image) && $request->banner_image != null) {

                $marketplace_image = $request->banner_image;

                Storage::disk('website_marketplace')->delete($request->banner_image);
                $extension = $marketplace_image->getClientOriginalExtension();
                $file_name = $request->slack . '_' . uniqid() . '.' . $extension;
                $path = Storage::disk('website_marketplace')->putFileAs('/', $marketplace_image, $file_name);
                $file_name = basename($path);

                WebsiteMarketplace::where('id', $marketplace->id)->update(['banner_image' => $file_name]);
            }

            $specifications = json_decode($request->specifications, true);

            if (isset($specifications)) {

                $specifications_array = [];
                foreach ($specifications as $specification) {
                    $dataset['marketplace_id'] = $marketplace->id;
                    $dataset['name'] = $specification['name'];
                    $dataset['name_ar'] = $specification['name_ar'];
                    $dataset['value'] = $specification['value'];
                    $dataset['value_ar'] = $specification['value_ar'];
                    $specifications_array[] = $dataset;
                }
            }

            WebsiteMarketplaceSpecification::where('marketplace_id', $marketplace->id)->forceDelete();
            WebsiteMarketplaceSpecification::insert($specifications_array);

            DB::commit();

            return response()->json($this->generate_response(
                array(
                    "message" => "Marketplace has been updated successfully!",
                    "data" => []
                ),
                'SUCCESS'
            ));
        } catch (Exception $e) {

            return response()->json($this->generate_response(
                array(
                    "message" => $e->getMessage(),
                    "status_code" => $e->getCode()
                )
            ));
        }
    }
}
