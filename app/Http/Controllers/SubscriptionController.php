<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use App\Http\Resources\SubscriptionResource;
use App\Models\MerchantMenu;
use App\Models\SubscriptionMenu;

class SubscriptionController extends Controller
{

    public function index()
    {
        check_access(['MM_SUBSCRIPTION', 'SM_SUBSCRIPTION', 'A_VIEW_SUBSCRIPTION']);

        $data['title'] = 'Subscriptions';

        // get all subscriptions
        $subscriptions = Subscription::all();
        $data['subscriptions'] = SubscriptionResource::collection($subscriptions);

        return view('subscription.list', compact('data'));
    }

    public function add($slack = null)
    {
        check_access(['MM_SUBSCRIPTION', 'SM_SUBSCRIPTION', 'A_ADD_SUBSCRIPTION']);

        $data['title'] = 'Add Subscription';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit Subscription';
            $data['edit_data'] = new SubscriptionResource(Subscription::with('features')->where('slack', $slack)->first());
        }

        return view('subscription.add', compact('data'));
    }

    public function show($slack)
    {
        check_access(['MM_SUBSCRIPTION', 'SM_SUBSCRIPTION', 'A_VIEW_SUBSCRIPTION']);
        $data['title'] = 'Subscription Detail';

        $subscription =  new SubscriptionResource(Subscription::where('slack', $slack)->first());
        $data['subscription'] = $subscription->toArray($subscription);

        return view('subscription.show', compact('data'));
    }

    public function menus($slack)
    {
        check_access(['MM_SUBSCRIPTION', 'SM_SUBSCRIPTION', 'A_VIEW_SUBSCRIPTION']);
        $data['title'] = 'Manage Subscription Permissions';

        $subscription = Subscription::where('slack', $slack)->first();
        $edit_data = new SubscriptionResource($subscription);
        $menus = SubscriptionMenu::where('subscription_id', $subscription->id)->pluck('menu_id')->toArray();
        $data['edit_data'] = collect($edit_data)->union(collect(['menus' => $menus]));

        $data['all_menus'] = MerchantMenu::active()->get();

        $data['subscription_slack'] = $slack;
        $data['merchant_menus'] = getMerchantMenus($subscription->id);

        return view('subscription.menu', compact('data'));
    }
}
