<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WebsiteReview;
use App\Http\Resources\WebsiteReviewResource;

class WebsiteReviewController extends Controller
{

    public function index()
    {
        $data['title'] = 'Reviews';

        // get all reviews
        $reviews = WebsiteReview::all();
        $data['reviews'] = WebsiteReviewResource::collection($reviews);

        return view('website.review.list', compact('data'));
    }

    public function add($slack = null)
    {

        $data['title'] = 'Add Review';
        $data['edit_data'] = null;
        if ($slack != null) {
            $data['title'] = 'Edit review';
            $data['edit_data'] = WebsiteReview::where('slack', $slack)->first();
        }
        return view('website.review.add', compact('data'));
    }
}
