<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;

class MenuController extends Controller
{

    use AuthenticatesUsers;

    public function menus()
    {

        $data['title'] = 'Login';

        return view('login', compact('data'));
    }
}
