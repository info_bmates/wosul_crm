<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:50'],
            'email' => ['required', 'email', Rule::unique('users')->ignore(request('id'))->where(function ($query) {
                $query->where('deleted_at', NULL);
            }), 'max:50'],
            'phone' => [Rule::unique('users')->ignore(request('id'))->where(function ($query) {
                $query->where('deleted_at', NULL);
            })],
            'password' => ['required'],
        ];
    }
}
