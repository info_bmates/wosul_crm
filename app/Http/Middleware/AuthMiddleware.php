<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        if (!Auth::user()) {
            return redirect('/login');
        }


        $menus = Menu::select('menus.*')->join('user_menus', 'user_menus.menu_id', 'menus.id')->where('user_menus.user_id', Auth::user()->id)->active()->get();

        View::share('logged_in_user', Auth::user());
        View::share('menus', $menus);

        return $next($request);
    }
}
