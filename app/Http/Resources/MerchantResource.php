<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class MerchantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slack' => $this->slack,
            'subscription_id' => $this->subscription_id,
            'name' => $this->name,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'password' => $this->password,
            'company_name' => $this->company_name,
            'company_url' => $this->company_url,
            'db_name' => Str::lower($this->company_url) . "_wosul",
            'company_url_full' => 'https://' . Str::lower($this->company_url) . ".wosul.sa",
            'address' => $this->address,
            'referral_code' => $this->referral_code,
            'recommendation' => $this->recommendation,
            'status' => $this->status,
            'status_text' => $this->status_text,
            'created_by' => $this->createdUser,
            'updated_by' => $this->updatedUser,
            'deleted_at_label' => $this->parseDate($this->created_at),
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at),
            /* relations */
            'registered_at' => Carbon::parse($this->created_at)->format('d M, Y'),
            'merchant_url' => 'https://'.Str::lower($this->company_url). ".wosul.sa",
            'merchant_subscription' => (isset($this->merchant_subscription)) ? new MerchantSubscriptionResource($this->merchant_subscription) : [],
        ];
    }
}
