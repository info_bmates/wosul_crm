<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'menu_key' => $this->menu_key,
            'label' => $this->label,
            'route' => $this->route,
            'parent' => $this->parent,
            'sort_order' => $this->sort_order,
            'icon' => $this->icon,
            'image' => $this->image,
            'is_restaurant_menu' => $this->is_restaurant_menu,
            'status' => $this->status,
            'deleted_at_label' => $this->parseDate($this->created_at),
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at),
            'submenus' => $this->submenus,
        ];
    }
}
