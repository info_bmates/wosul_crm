<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WhatsappVisitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $tracking_data = [];   
        if(!is_null($this->tracking_data)){
            $tracking_data = json_decode($this->tracking_data,true);
        }

        return [
            'id' => $this->id,
            'ip' => $this->ip,
            'tracking_data' => $this->tracking_data,
            'city_name' => $tracking_data['cityName'] ?? '',
            'region_name' => $tracking_data['regionName'] ?? '',
            'country_name' => $tracking_data['countryName'] ?? '',
            'timezone' => $tracking_data['timezone'] ?? '',
            'latitude' => $tracking_data['latitude'] ?? '',
            'longitude' => $tracking_data['longitude'] ?? '',
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at),
        ];
    }
}
