<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'slack' => $this->slack,
            'name' => $this->name,
            'email' => $this->email,
            'is_email_verified_text' => $this->email_verified_text,
            'password' => $this->password,
            'phone' => $this->phone,
            'is_phone_verified_text' => $this->phone_verified_text,
            'role' => new RoleResource($this->role),
            'status' => $this->status,
            'status_text' => $this->status_text,
            'is_admin' => $this->is_admin,
            'is_admin_text' => $this->is_admin_text,
            'created_by' => $this->createdUser,
            'updated_by' => $this->updatedUser,
            'deleted_at_label' => $this->parseDate($this->created_at),
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at),
        ];
    }
}
