<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebsiteReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slack' => $this->slack,
            'image' => $this->image,
            'image_path' => $this->image_path,
            'name' => $this->name,
            'name_ar' => $this->name_ar,
            'review' => $this->review,
            'review_ar' => $this->review_ar,
            'rating' => $this->rating,
            'status' => $this->status,
            'status_text' => $this->status_text,
            'created_by' => $this->createdUser,
            'updated_by' => $this->updatedUser,
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at)
        ];
    }
}
