<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slack' => $this->slack,
            'title' => $this->title,
            'title_ar' => $this->title_ar,
            'short_description' => $this->short_description,
            'short_description_ar' => $this->short_description_ar,
            'subscription_type' => $this->subscription_type,
            'subscription_tenure' => $this->subscription_tenure,
            'currency' => $this->currency,
            'amount' => $this->amount,
            'discount' => $this->discount,
            'discount_description' => $this->discount_description,
            'discount_description_ar' => $this->discount_description_ar,
            'is_featured' => $this->is_featured,
            'status' => $this->status,
            'status_text' => $this->status_text,
            'created_by' => $this->createdUser,
            'updated_by' => $this->updatedUser,
            'created_at_label' => $this->parseDate($this->created_at),
            'updated_at_label' => $this->parseDate($this->updated_at),
            'features' => $this->features,
            'subscription_menus' => $this->subscription_menus,
        ];
    }
}
